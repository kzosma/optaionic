import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OptaConstants } from '../constants/opta-constants';


// CONSTANTES
const CLOUDANT_IAM_API_KEY = OptaConstants.cloudant_iam_api_key;
const CLOUDANT_URL = OptaConstants.cloudant_url;
const CLOUDANT_AUTH = `Bearer ${CLOUDANT_IAM_API_KEY}`;

@Injectable({
  providedIn: 'root'
})
export class CloudantService {


  // HTTP HEADERS
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${CLOUDANT_IAM_API_KEY}`
    })
  };
  
  // CONSTRUCTOR
  constructor(private httpClient: HttpClient) {}
  
  getDoc(db: string, docId: string): Observable<{}>  {
    const url = `${CLOUDANT_URL}/${db}/${docId}`;
    return this.httpClient.get<{}>(url, this.httpOptions);
  }
}
