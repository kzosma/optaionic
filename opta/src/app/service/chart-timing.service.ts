import { Injectable } from '@angular/core';
import { OptaApiService } from '../service/opta-api.service';


@Injectable({
  providedIn: 'root'
})
export class ChartTimingService {

  // SERVICES
  optaApi: OptaApiService;
  constructor(private optaapi: OptaApiService) {
    this.optaApi = optaapi;
  }
  
  /* PROCESS RESUME */
  processResumeTeam = (idDoc: string, season: string, dbname: string):Promise<any> => {
    const body = {"season":season, "doc":idDoc, "dbname":dbname};
    console.log(body);
    const p = new Promise<string>((resolve, reject) => {
      this.optaApi.getOptaInfoResume(body).subscribe((data: any) => 
        {
          resolve(data);
        }, err => {
          resolve(null);
        });
      });
      return p;
  }
  
  processStatsTeam = (idTeam: string, season: number):Promise<any> => {
    const body = {"season":season, "idteam":idTeam};
    const p = new Promise<string>((resolve, reject) => {
      console.log('body : ', body);
      this.optaApi.getOptaInfoTeam(body).subscribe((data: any) => 
        {
          console.log('data : ', data);
          resolve(data);
        }, err => {
          resolve(null);
        });
      });
      return p;
  }
  
  // GET Timing keys
  getKeysTiming = (team: any) => {
    let labels = [];
    Object.keys(team.timing).map(function(key){  
      labels.push(key);  
      return labels;  
    });  
    return labels;
  }

  // GET Goals by Timing key
  getGoalsTimingByKey = (team: any) => {
    let goals = [];  
    Object.keys(team.timing).map(function(key){  
      goals.push(team.timing[key].goals); 
      return goals;  
    });  
    return goals;
  }

  // GET Goals fixture for each Timing Key
  getEvolutionGoalsByFixtures = (team: any, timingKey: string) => {
    let goals = [];  
    team.fixtures.forEach(fixture => {
      if(fixture.timing !== undefined) {
        Object.keys(fixture.timing).map(function(key){  
          if(key === timingKey) {
            goals.push(fixture.timing[key].goals); 
          }
        }); 
      }
    });
    return goals;
  }
   // Construction modèle de données Global Timing
   buildDataGlobalTiming = (home: any, away: any): any => {
    let globalData = {
      'id': '',
      'header': '',
      'data': {},
      'options': {}
     };
   
    let lastMatchHome = home.fixtures[home.fixtures.length - 1];
    let lastMatchAway = away.fixtures[away.fixtures.length - 1];
    
    globalData.data = {
      labels: this.getKeysTiming(lastMatchHome),
      datasets: [
          {
              label: home.title,
              data: this.getGoalsTimingByKey(lastMatchHome),
              borderColor: '#42A5F5',
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
              label: away.title,
              data: this.getGoalsTimingByKey(lastMatchAway),
              borderColor: '#FFA726',
              backgroundColor: '#FFFFFF',
              tension: .4
          }
      ]
    };
    globalData.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return globalData;
  }

  // Construction modèle de données Team Data Timing
  buildDataTimingTeam = (team: any, id: string, header: string): any => {

    let teamData = {
      'id': id,
      'header': header,
      'data': {},
      'options': {}
     };
    let oneMatchTeam = team.fixtures[0];
    const labels = [];
    team.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    console.log('debug : ', team);
    const keys = this.getKeysTiming(oneMatchTeam); 
    let datasets = [];
    const colors = ['#23C077','#E39967','#6767E3','#DE3ED3','#E6461E','#000000'];
    keys.forEach((key, index) => {
        datasets.push(
          {
            label: key,
            data: this.getEvolutionGoalsByFixtures(team, key),
            borderColor: colors[index],
            backgroundColor: '#FFFFFF',
            tension: .4
        }
        );
    });
    teamData.data = {
      labels: labels,
      datasets: datasets
    };
    teamData.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return teamData;
  }

}
