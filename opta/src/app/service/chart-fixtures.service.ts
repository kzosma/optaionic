import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChartFixturesService {

  constructor() {}

  // Construction modèle de données Global Fixtures
  buildDataGlobalFixtures = (home: any, away: any): any => {
    let fixturesDistance = {
      'id': '',
      'header': '',
      'data': {},
      'options': {}
     };
    const labels = [];
    home.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    fixturesDistance.data = {
      labels: labels,
      datasets: [
          {
              label: this.spliTeamName(home.title),
              data: this.getDistanceForTeam(home),
              borderColor: '#42A5F5',
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
              label: this.spliTeamName(away.title),
              data: this.getDistanceForTeam(away),
              borderColor: '#FFA726',
              backgroundColor: '#FFFFFF',
              tension: .4
          }
      ]
    };
    fixturesDistance.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return fixturesDistance;
  }

  buildDataGlobalFixturesGoalFor = (team: any): any => {
    let fixturesGoalFor = {
      'id': 'goal_for',
      'header': 'Fixtures Goals For',
      'data': {},
      'options': {}
     };
    const labels = [];
    team.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    fixturesGoalFor.data = {
      labels: labels,
      datasets: [
          {
              label: "Goal For",
              data: this.getGoalForTeam(team),
              borderColor: '#0000e6',
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
            label: "Goal Against",
            data: this.getGoalAgainstTeam(team),
            borderColor: '#e6005c',
            backgroundColor: '#FFFFFF',
            tension: .4
        }
      ]
    };
    fixturesGoalFor.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return fixturesGoalFor;
  }

  buildDataGlobalFixturesAttDefTeam = (team: any): any => {
    let fixturesGoalFor = {
      'id': 'att_def_for',
      'header': 'Attaque Pour et Contre',
      'data': {},
      'options': {}
     };
    const labels = [];
    team.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    fixturesGoalFor.data = {
      labels: labels,
      datasets: [
          {
              label: "Attaque For",
              data: this.getAttForTeam(team),
              borderColor: '#2d862d',
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
              label: "Attaque Against",
              data: this.getAttAgainstTeam(team),
              borderColor: '#e6005c',
              backgroundColor: '#FFFFFF',
              tension: .4
          }
      ]
    };
    fixturesGoalFor.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return fixturesGoalFor;
  }

   /* Build Fixtures XGBuildup */
   buildFixturesXGBuildup = (home: any, away: any) => {

    let fixturesXGBuildup = {
      'id': 'fixtures_xgbuildup',
      'header': 'Fixtures XGBuildup',
      'data': {},
      'options': {}
     };
    const labels = [];
    home.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    fixturesXGBuildup.data = {
      labels: labels,
      datasets: [
          {
              label: this.spliTeamName(home.title) + "(Def)",
              data: {},
              borderColor: '#2d862d',
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
            label: this.spliTeamName(home.title) + "(Mil)",
            data: {},
            borderColor: '#2d862d',
            backgroundColor: '#FFFFFF',
            tension: .4
        },
        {
          label: this.spliTeamName(home.title) + "(Att)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
        },
        {
          label: this.spliTeamName(home.title) + "(Sub)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
        },
        {
          label: this.spliTeamName(home.title) + "(Total)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
      }
          
      ]
    };
    fixturesXGBuildup.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return fixturesXGBuildup;

   }

   /* Build Fixtures XGChain */
   buildFixturesXGChain = (home: any, away: any) => {

    let fixturesXGChain = {
      'id': 'fixtures_xgchain',
      'header': 'Fixtures XGChain',
      'data': {},
      'options': {}
     };
    const labels = [];
    home.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    fixturesXGChain.data = {
      labels: labels,
      datasets: [
          {
              label: this.spliTeamName(home.title) + "(Def)",
              data: {},
              borderColor: '#2d862d',
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
            label: this.spliTeamName(home.title) + "(Mil)",
            data: {},
            borderColor: '#2d862d',
            backgroundColor: '#FFFFFF',
            tension: .4
        },
        {
          label: this.spliTeamName(home.title) + "(Att)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
        },
        {
          label: this.spliTeamName(home.title) + "(Sub)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
        },
        {
          label: home.title + "(Total)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
      }
          
      ]
    };
    fixturesXGChain.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return fixturesXGChain;


   }

   /* Build Fixtures Delta Goals XG */
   buildFixturesDeltaGoalsXG = (home: any, away: any) => {

    let fixturesDeltaGoalsXG = {
      'id': 'fixtures_delta_goals_xg',
      'header': 'Fixtures Delta Goals XG',
      'data': {},
      'options': {}
     };
    const labels = [];
    home.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    fixturesDeltaGoalsXG.data = {
      labels: labels,
      datasets: [
          {
              label: this.spliTeamName(home.title) + "(Def)",
              data: {},
              borderColor: '#2d862d',
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
            label: this.spliTeamName(home.title) + "(Mil)",
            data: {},
            borderColor: '#2d862d',
            backgroundColor: '#FFFFFF',
            tension: .4
        },
        {
          label: this.spliTeamName(home.title) + "(Att)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
        },
        {
          label: this.spliTeamName(home.title) + "(Sub)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
        },
        {
          label: this.spliTeamName(home.title) + "(Total)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
      }
          
      ]
    };
    fixturesDeltaGoalsXG.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return fixturesDeltaGoalsXG;

   }
  
   /* Build Fixtures Goals On Shots */
   buildFixturesGoalsOnShots = (home: any, away: any) => {

    let fixturesGoalsOnShots = {
      'id': 'fixtures_goals_on_shots',
      'header': 'Fixtures Goals On Shots',
      'data': {},
      'options': {}
     };
    const labels = [];
    home.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    fixturesGoalsOnShots.data = {
      labels: labels,
      datasets: [
          {
              label: this.spliTeamName(home.title) + "(Def)",
              data: {},
              borderColor: '#2d862d',
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
            label: this.spliTeamName(home.title) + "(Mil)",
            data: {},
            borderColor: '#2d862d',
            backgroundColor: '#FFFFFF',
            tension: .4
        },
        {
          label: home.title + "(Att)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
        },
        {
          label: home.title + "(Sub)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
        },
        {
          label: home.title + "(Total)",
          data: {},
          borderColor: '#2d862d',
          backgroundColor: '#FFFFFF',
          tension: .4
      }
          
      ]
    };
    fixturesGoalsOnShots.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return fixturesGoalsOnShots;
   }
  
   /* Build Fixtures Sorare Points */
   buildFixturesSorarePoints = (home: any, away: any, header: string, id: string, type: string) => {

    let fixturesSorarePoints = {
      'id': id,
      'header': header,
      'data': {},
      'options': {}
     };
    const labels = [];
    home.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    fixturesSorarePoints.data = {
      labels: labels,
      datasets: [
          {
              label: this.spliTeamName(home.nameTeam),
              data: this.getSorarePointsTeam(home, type),
              borderColor: '#F01347',
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
            label: this.spliTeamName(away.nameTeam),
            data: this.getSorarePointsTeam(away, type),
            borderColor: '#4118A1',
            backgroundColor: '#FFFFFF',
            tension: .4
        }
      ]
    };
    fixturesSorarePoints.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return fixturesSorarePoints;
  }

  /* Build Game State Time */
  buildGameStateTime = (home: any, header: string, id: string, type: string) => {

    let gameStateData = {
      'id': id,
      'header': header,
      'data': {},
      'options': {}
     };
    const labels = [];
    home.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    gameStateData.data = {
      labels: labels,
      datasets: [
          {
              label: 'Time_0',
              type: 'line',
              borderColor: '#42A5F5',
              borderWidth: 2,
              fill: false,
              data: this.getGameStateTimeTeam(home,'zero'),
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
            label: 'Time_NEG',
            type: 'line',
            borderColor: '#de0909',
            borderWidth: 2,
            fill: false,
            data: this.getGameStateTimeTeam(home,'neg'),
            backgroundColor: '#FFFFFF',
            tension: .4
        },
        {
          label: 'Time_POS',
          type: 'line',
          borderColor: '#05731c',
          borderWidth: 2,
          fill: false,
          data: this.getGameStateTimeTeam(home,'pos'),
          backgroundColor: '#FFFFFF',
          tension: .4
        }
      ]
    };
    gameStateData.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return gameStateData;

   }

    /* Build Game State Goals */
  buildGameStateGoals = (home: any, header: string, id: string, type: string) => {

    let gameStateGoals = {
      'id': id,
      'header': header,
      'data': {},
      'options': {}
     };
    const labels = [];
    home.fixtures.forEach((fixt, index) => {
      labels.push(index+1);
    });
    gameStateGoals.data = {
      labels: labels,
      datasets: [
          {
              label: 'FOR_'+type,
              type: 'line',
              borderColor: '#42A5F5',
              borderWidth: 2,
              fill: false,
              data: this.getGameStateGoalsTeam(home,'for_'+type),
              backgroundColor: '#FFFFFF',
              tension: .4
          },
          {
            label: 'AGAINST_'+type,
            type: 'line',
            borderColor: '#de0909',
            borderWidth: 2,
            fill: false,
            data: this.getGameStateGoalsTeam(home,'against_'+type),
            backgroundColor: '#FFFFFF',
            tension: .4
        }
      ]
    };
    gameStateGoals.options = {
      title: {
          display: true,
          text: '',
          fontSize: 16
      },
      legend: {
          position: 'bottom'
      }
  };
  return gameStateGoals;

   }
  

  /* GETTERS */


  /* GET DISTANCE */
  getDistanceForTeam = (team: any) => {
    let distances = [];  
    team.fixtures.forEach((fixture) => {
      distances.push(fixture.distanceTeam);
    });
    return distances;
  }
  /* GET GOALS FOR */
  getGoalForTeam = (team: any) => {
    let goals_for = [];  
    team.fixtures.forEach((fixture) => {
      goals_for.push(fixture.g_for);
    });
    return goals_for;
  }
   /* GET GOALS AGAINST */
   getGoalAgainstTeam = (team: any) => {
    let goals_against = [];  
    team.fixtures.forEach((fixture) => {
      goals_against.push(fixture.g_against);
    });
    return goals_against;
  }
  /* GET ATT FOR */
  getAttForTeam = (team: any) => {
    let att_for = [];  
    team.fixtures.forEach((fixture) => {
      att_for.push(fixture.att_for);
    });
    return att_for;
  }
  /* GET ATT AGAINST */
  getAttAgainstTeam = (team: any) => {
    let att_against = [];  
    team.fixtures.forEach((fixture) => {
      att_against.push(fixture.att_against);
    });
    return att_against;
  }
    /* GET DEF FOR */
    getDefForTeam = (team: any) => {
      let def_for = [];  
      team.fixtures.forEach((fixture) => {
        def_for.push(fixture.def_for);
      });
      return def_for;
    }
    /* GET DEF AGAINST */
    getDefAgainstTeam = (team: any) => {
      let def_against = [];  
      team.fixtures.forEach((fixture) => {
        def_against.push(fixture.def_against);
      });
      return def_against;
    }

    /* GET SORARE POINTS */
    getSorarePointsTeam = (team: any, position: any) => {
      let sorare_points = []; 
      if(position === 'att') {
        team.fixtures.forEach((fixture) => {
          sorare_points.push(fixture.sorareData.att);
        });
      }
      else if(position === 'mil') {
        team.fixtures.forEach((fixture) => {
          sorare_points.push(fixture.sorareData.mil);
        });
      }
      else if(position === 'att_mil') {
        team.fixtures.forEach((fixture) => {
          sorare_points.push(fixture.sorareData.att_mil);
        });
      }
      else if(position === 'def') {
        team.fixtures.forEach((fixture) => {
          sorare_points.push(fixture.sorareData.def);
        });
      }
      else if(position === 'sub') {
        team.fixtures.forEach((fixture) => {
          sorare_points.push(fixture.sorareData.sub);
        });
      } 
      else if(position === 'total') {
        team.fixtures.forEach((fixture) => {
          sorare_points.push(fixture.sorareData.total);
        });
      }
      
      return sorare_points;
    }

    /* GET GAME STATE DATA */
    getGameStateTimeTeam = (team: any, mode?: string ) => {
      let game_state_time = []; 
      let time_total = 0;
      team.fixtures.forEach((fixture,index) => {
        const gameState = fixture.gameState;
        if(gameState !== undefined) {
          time_total = 90 *(index +1);
        if(mode === 'zero' && gameState["Goal diff 0"]) {
          if(gameState["Goal diff 0"].time < time_total) {
            game_state_time.push(this.calculateGameStateData(gameState["Goal diff 0"].time, time_total,2));
          }
          else {
            game_state_time.push(game_state_time[index-1]);
          }
        }
        if(mode === 'neg') {
          let time_neg = 0;
          if(gameState["Goal diff -1"]) {
            time_neg = time_neg + gameState["Goal diff -1"].time;
          }
          if(gameState["Goal diff < -1"]) {
            time_neg = time_neg + gameState["Goal diff < -1"].time;
          }
          if(time_neg < time_total) {
            game_state_time.push(this.calculateGameStateData(time_neg, time_total,2));
          }
          else {
            game_state_time.push(game_state_time[index-1]);
          }
          
        }
        if(mode === 'pos') {
          let time_pos = 0;
          if(gameState["Goal diff +1"]) {
            time_pos = time_pos + gameState["Goal diff +1"].time;
          }
          if(gameState["Goal diff > +1"]) {
            time_pos = time_pos + gameState["Goal diff > +1"].time;
          }
          if(time_pos < time_total) {
            game_state_time.push(this.calculateGameStateData(time_pos, time_total,2));
          }
          else {
            game_state_time.push(game_state_time[index-1]);
          }
        }
      }
     });
      return game_state_time;
    }

    getGameStateGoalsTeam = (team: any, mode?: string) => {
      let game_state_shots = []; 
      team.fixtures.forEach((fixture,index) => {
        const gameState = fixture.gameState;
        if(gameState !== undefined) {
          if(mode === 'for_0' && gameState["Goal diff 0"]) {
            game_state_shots.push(this.calculateGameStateData(gameState["Goal diff 0"].goals, gameState["Goal diff 0"].shots,2));
          }
          if(mode === 'against_0' && gameState["Goal diff 0"]) {
            game_state_shots.push(this.calculateGameStateData(gameState["Goal diff 0"].against.goals, gameState["Goal diff 0"].against.shots,2));
          }
          if(mode === 'for_more') {
            let shots_pos = 0;
            let goals_pos = 0;
            if(gameState["Goal diff +1"]) {
              shots_pos = shots_pos + gameState["Goal diff +1"].shots;
              goals_pos = goals_pos + gameState["Goal diff +1"].goals;
            }
            if(gameState["Goal diff > +1"]) {
              shots_pos = shots_pos + gameState["Goal diff > +1"].shots;
              goals_pos = goals_pos + gameState["Goal diff > +1"].goals;
            }
            game_state_shots.push(this.calculateGameStateData(goals_pos, shots_pos,2));
           
          }
          if(mode === 'against_more') {
            let shots_pos = 0;
            let goals_pos = 0;
            if(gameState["Goal diff +1"]) {
              shots_pos = shots_pos + gameState["Goal diff +1"].against.shots;
              goals_pos = goals_pos + gameState["Goal diff +1"].against.goals;
            }
            if(gameState["Goal diff > +1"]) {
              shots_pos = shots_pos + gameState["Goal diff > +1"].against.shots;
              goals_pos = goals_pos + gameState["Goal diff > +1"].against.goals;
            }
            game_state_shots.push(this.calculateGameStateData(goals_pos, shots_pos,2));
           
          }
  
          // LESS
          if(mode === 'for_less') {
            let shots_pos = 0;
            let goals_pos = 0;
            if(gameState["Goal diff -1"]) {
              shots_pos = shots_pos + gameState["Goal diff -1"].shots;
              goals_pos = goals_pos + gameState["Goal diff -1"].goals;
            }
            if(gameState["Goal diff < -1"]) {
              shots_pos = shots_pos + gameState["Goal diff < -1"].shots;
              goals_pos = goals_pos + gameState["Goal diff < -1"].goals;
            }
            game_state_shots.push(this.calculateGameStateData(goals_pos, shots_pos,2));
           
          }
          if(mode === 'against_less') {
            let shots_pos = 0;
            let goals_pos = 0;
            if(gameState["Goal diff -1"]) {
              shots_pos = shots_pos + gameState["Goal diff -1"].against.shots;
              goals_pos = goals_pos + gameState["Goal diff -1"].against.goals;
            }
            if(gameState["Goal diff < -1"]) {
              shots_pos = shots_pos + gameState["Goal diff < -1"].against.shots;
              goals_pos = goals_pos + gameState["Goal diff < -1"].against.goals;
            }
            game_state_shots.push(this.calculateGameStateData(goals_pos, shots_pos,2));
          }
        }
       });
      return game_state_shots;
    }
    spliTeamName = (name: string):string => {
      return name.split('_')[0];
    }

    calculateGameStateData = (num: any, den: any, fractionDigits: number): number => {
      if(typeof num === 'string') {
        return Number((100*(+num)/den).toFixed(fractionDigits));
      }
      else if(typeof num === 'number') {
        return Number((100*num/den).toFixed(fractionDigits));
      }
      else {
        return 0;
      }
    }

    

}
