import { Injectable } from '@angular/core';
import { OptaApiService } from './opta-api.service';

@Injectable({
  providedIn: 'root'
})
export class ChartWhoScoredService {

  // SERVICES
  optaApi: OptaApiService;
  constructor(private optaapi: OptaApiService) {
    this.optaApi = optaapi;
  }

  processStatsWhoScored = (idTeam: string, season: number):Promise<any> => {
    const body = {"season":season, "doc":idTeam};
    const p = new Promise<string>((resolve, reject) => {
      this.optaApi.getOptaInfoWhoScored(body).subscribe((resp: any) => 
        {
          resolve(resp.data);
        }, err => {
          resolve(null);
        });
      });
      return p;
  }

}
