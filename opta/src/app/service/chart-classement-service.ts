import { Injectable } from '@angular/core';
import { OptaApiService } from './opta-api.service';

@Injectable({
  providedIn: 'root'
})
export class ChartClassementService {

  // SERVICES
  optaApi: OptaApiService;
  constructor(private optaapi: OptaApiService) {
    this.optaApi = optaapi;
  }

  processStatsClassements = (doc_classement: string, season: number):Promise<any> => {
    const body = {"season":season, "doc":doc_classement};
    const p = new Promise<string>((resolve, reject) => {
      this.optaApi.getOptaInfoClassements(body).subscribe((resp: any) => 
        {
          resolve(resp.data);
        }, err => {
          resolve(null);
        });
      });
      return p;
  }
}
