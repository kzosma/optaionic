import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { OptaConstants } from '../constants/opta-constants';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OptaApiService {

  httpClient: HttpClient;
  constructor(httpclient: HttpClient) {
    this.httpClient = httpclient;
   }
   
   getOptaInfoResume = (body: any): Observable<any> => {
    return this.httpClient.post(OptaConstants.base_url_optadb_resume, body, {responseType: 'json'});
   };
   
   getOptaInfoIonic = (body: any): Observable<any> => {
      return this.httpClient.post(OptaConstants.base_url_optadb_ionic, body, {responseType: 'json'});
   };
    getOptaInfoTeam = (body: any): Observable<any> => {
      return this.httpClient.post(OptaConstants.base_url_opta_info_by_team, body, {responseType: 'json'});
    };
    getOptaInfoWhoScored = (body: any): Observable<any> => {
      return this.httpClient.post(OptaConstants.base_url_optadb_whoscored, body, {responseType: 'json'});
   };
   getOptaInfoClassements = (body: any): Observable<any> => {
    return this.httpClient.post(OptaConstants.base_url_optadb_classements, body, {responseType: 'json'});
   };
   getOptaInfoSoccerStats = (body: any): Observable<any> => {
    return this.httpClient.post(OptaConstants.base_url_optadb_soccerstats, body, {responseType: 'json'});
   };
   getOptaInfoStat = (body: any): Observable<any> => {
    return this.httpClient.post(OptaConstants.base_url_optadb_infostats, body, {responseType: 'json'});
   };
   getOptaGenData = (body: any): Observable<any> => {
    return this.httpClient.post(OptaConstants.base_url_optadb_gendata, body, {responseType: 'json'});
   };

}
