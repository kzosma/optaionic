import { Component, OnInit } from '@angular/core';
import { OptaApiService } from '../service/opta-api.service';
import { OptaConstants } from '../constants/opta-constants';
import { ChartClassementService } from '../service/chart-classement-service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-data-stats',
  templateUrl: './data-stats.page.html',
  styleUrls: ['./data-stats.page.scss'],
})
export class DataStatsPage implements OnInit {

  // Styles
  stylesCSS: any = {'background':'#FFFFFF', 'color': 'black', 'margin-left': '15px', 'margin-right': '15px'};

  // SERVICES
  optaApi: OptaApiService;
  chartClassements: ChartClassementService;
  // Data
  alldataclassements = [];
  alldata_head_head = [];
  leagues = [];
  league_selected = '';
  teams_selected = [];
  matches_selected = [];
  match_selected: any = {};
  home_selected: any = undefined;
  away_selected: any = undefined;
  
  HaFormeLeaguesMap: Record<string, any[]> = {};
  HomeFormeLeaguesMap: Record<string, any[]> = {};
  AwayFormeLeaguesMap: Record<string, any[]> = {};
  HaRepereLeagueMap: Record<string, any[]> = {};
  HaScorerLeagueMap: Record<string, any[]> = {};


  constructor(private optaapi: OptaApiService, private loadingCtrl: LoadingController) {
    this.optaApi = optaapi;
   }
   ngOnInit() {
    this.loadLeagues();
  }
  selectLeague = (league_selected: string) => {
    this.initData();
    this.teams_selected = this.alldataclassements.find(obj => obj.id === league_selected);
    this.matches_selected = this.alldata_head_head.filter(obj => obj.id.startsWith(league_selected+'-'));
  }
  /* GET API */
  async getDataFromApis() {
    try {
      const [data1,data2] = await Promise.all([
        this.getApiAllDataClassement(),
        this.getApiAllDataHeadToHead()
      ]);
      this.handleResponseApiAllDataClassement(data1);
      this.handleResponseApiAllDataHeadToHead(data2);
    } catch (error) {
      console.error('Une erreur s\'est produite :', error);
    }
  }

  private async getApiAllDataClassement(): Promise<any> {
    const requestClassement = {"dbname": OptaConstants.dbname_fbref_classement};
    return this.optaApi.getOptaGenData(requestClassement).toPromise();
  }

  private handleResponseApiAllDataClassement(data: any) {
    this.alldataclassements = data;
    for (let l of this.alldataclassements) {
      this.leagues.push(l.id);
    }
    this.allDataLeaguesResume();
  }

  private async getApiAllDataHeadToHead(): Promise<any> {
    const request_head_to_head = {"dbname": OptaConstants.dbname_fbref_head_to_head};
      return this.optaApi.getOptaGenData(request_head_to_head).toPromise();
  }

  private handleResponseApiAllDataHeadToHead(data: any) {
    console.log('Response AllDataHeadToHead  : ', data);
    this.alldata_head_head = data;
    for(const docHead of this.alldata_head_head) {
      const leagueid = docHead.id.split('-')[0];
      if(!this.HaRepereLeagueMap[leagueid]) {
        this.HaRepereLeagueMap[leagueid] = [];
      }
      if(!this.HaScorerLeagueMap[leagueid]) {
        this.HaScorerLeagueMap[leagueid] = [];
      }
      const home = docHead.data.home;
      const away = docHead.data.away;
      let home_team = {'name': home.team, 'reperes': []};
      let away_team = {'name': away.team, 'reperes': []};

      // Cinq derniers matchs de l'équipe HOME
      const home_last5 = home.last_h_a.slice(0,5);
      let home_players_last5: Record<string, any> = {};;
      for (let dh of home_last5) {
        for(let p of dh.roster) {
          if(!home_players_last5[p.name]) {
            home_players_last5[p.name] = {'W': 0, 'D': 0, 'L': 0, 'goals':0, 'assists': 0};
          }
          if(dh.result === 'W') {
            home_players_last5[p.name].W = home_players_last5[p.name].W + 1;  
          }
          else if(dh.result === 'D') {
            home_players_last5[p.name].D = home_players_last5[p.name].D + 1;  
          }
          else if(dh.result === 'L') {
            home_players_last5[p.name].L = home_players_last5[p.name].L + 1;  
          }
          home_players_last5[p.name].goals = home_players_last5[p.name].goals + parseInt(p.goals);
          home_players_last5[p.name].assists = home_players_last5[p.name].assists + parseInt(p.assists);
        }
      }

      // Cinq derniers matchs de l'équipe AWAY
      const away_last5 = away.last_h_a.slice(0,5);
      let away_players_last5: Record<string, any> = {};;
      for (let da of away_last5) {
        for(let p of da.roster) {
          if(!away_players_last5[p.name]) {
            away_players_last5[p.name] = {'W': 0, 'D': 0, 'L': 0, 'goals':0, 'assists': 0};
          }
          if(da.result === 'W') {
            away_players_last5[p.name].W = away_players_last5[p.name].W + 1;  
          }
          else if(da.result === 'D') {
            away_players_last5[p.name].D = away_players_last5[p.name].D + 1;  
          }
          else if(da.result === 'L') {
            away_players_last5[p.name].L = away_players_last5[p.name].L + 1;  
          }
          away_players_last5[p.name].goals = away_players_last5[p.name].goals + parseInt(p.goals);
          away_players_last5[p.name].assists = away_players_last5[p.name].assists + parseInt(p.assists);
        }
      }
      //console.log('home_players_last5 : ', home_players_last5);
      // console.log('away_players_last5 : ', away_players_last5);

      for(const name in home.players) {
        const player_h = home_players_last5[name];
        let goals_last5 = 0;
        let assists_last5 = 0;
        if(player_h !== undefined) {
          goals_last5 =  player_h.goals;
          assists_last5 =  player_h.assists; 
        }
        const player_home = {
         'team': home.team, 
         'name': name,
         'position': home.players[name].position,
         'mp_per': home.players[name].percentage_matches_played,
         'mp_time': home.players[name].percentage_time_played,
         'mp_per_last': home.players[name].last_percentage_matches_played,
         'mp_time_last': home.players[name].last_percentage_time_played,
         'goals_last5': goals_last5,
         'assists_last5': assists_last5, 
         'status': ''
          
        };
        // Missing
        if(player_home.mp_per > player_home.mp_per_last && Math.abs(player_home.mp_per - player_home.mp_per_last) > 30) {
          player_home.status = 'missing';
          //home_team.reperes.push(player_home);
          this.HaRepereLeagueMap[leagueid].push(player_home);
        }
        // Increase
        else if(player_home.mp_per < player_home.mp_per_last && Math.abs(player_home.mp_per - player_home.mp_per_last) > 30) {
          player_home.status = 'increase';
          // home_team.reperes.push(player_home);
          this.HaRepereLeagueMap[leagueid].push(player_home);
  
        }
      }
      for(const name in away.players) {
        const player_a = away_players_last5[name];
        let goals_last5 = 0;
        let assists_last5 = 0;
        if(player_a !== undefined) {
          goals_last5 =  player_a.goals;
          assists_last5 =  player_a.assists; 
        }
        const player_away = {
          'team': away.team,
          'name': name,
          'position': away.players[name].position,
          'mp_per': away.players[name].percentage_matches_played,
          'mp_time': away.players[name].percentage_time_played,
          'mp_per_last': away.players[name].last_percentage_matches_played,
          'mp_time_last': away.players[name].last_percentage_time_played,
          'goals_last5': goals_last5,
          'assists_last5': assists_last5, 
          'status': ''
        };
        // Missing
        if(player_away.mp_per > player_away.mp_per_last && Math.abs(player_away.mp_per - player_away.mp_per_last) > 30) {
          player_away.status = 'missing';
          // away_team.reperes.push(player_away);
          this.HaRepereLeagueMap[leagueid].push(player_away);
  
        }
        // Increase
        else if(player_away.mp_per < player_away.mp_per_last && Math.abs(player_away.mp_per - player_away.mp_per_last) > 30) {
          player_away.status = 'increase';
          // away_team.reperes.push(player_away);
          this.HaRepereLeagueMap[leagueid].push(player_away);
        }
      }
      // Scorers
      let home_scorer = {'name': home.team,'opposite': away_team, 'scorer': []};
      let away_scorer = {'name': away.team, 'opposite': home_team, 'scorer': []};
      
      for(const scorer of home.scorer) {
        const player_h = home_players_last5[scorer.name_team];
        let goals_last5 = 0;
        let assists_last5 = 0;
        if(player_h !== undefined) {
          goals_last5 =  player_h.goals;
          assists_last5 =  player_h.assists; 
        }
        const scorer_home = {
         'team': home.team, 
         'name': scorer.name_team,
         'position': scorer.position,
         'assists':scorer.assists,
         'cards_red':scorer.cards_red,
         'cards_yellow':scorer.cards_yellow,
         'games':scorer.games,
         'goals':scorer.goals,
         'minutes':scorer.minutes,
         'goals_last5': goals_last5,
         'assists_last5': assists_last5 
        };
        this.HaScorerLeagueMap[leagueid].push(scorer_home);
        // home_scorer.scorer.push(scorer_home);
      }
      for(const scorer of away.scorer) {
        const player_a = away_players_last5[scorer.name_team];
        let goals_last5 = 0;
        let assists_last5 = 0;
        if(player_a !== undefined) {
          goals_last5 =  player_a.goals;
          assists_last5 =  player_a.assists; 
        }
        const scorer_away = {
         'team': away.team, 
         'name': scorer.name_team,
         'position': scorer.position,
         'assists':scorer.assists,
         'cards_red':scorer.cards_red,
         'cards_yellow':scorer.cards_yellow,
         'games':scorer.games,
         'goals':scorer.goals,
         'minutes':scorer.minutes,
         'goals_last5': goals_last5,
         'assists_last5': assists_last5 
        };
        this.HaScorerLeagueMap[leagueid].push(scorer_away);
        // away_scorer.scorer.push(scorer_away);
      }
      // this.HaScorerLeagueMap[leagueid].push(home_scorer);
      // this.HaScorerLeagueMap[leagueid].push(away_scorer);
    }
    console.log('Reperes : ', this.HaRepereLeagueMap);
    console.log('Scorers : ', this.HaScorerLeagueMap);
  }
  
  loadLeagues = () => {
    this.getDataFromApis();
  }

  /* private Methods */
  extractTeamNames(matchId: string): string {
    const matchIdParts = matchId.split('-');
    if (matchIdParts.length === 3) {
      return matchIdParts[1] + '-' + matchIdParts[2];
    } else {
      return matchId;
    }
  }
  selectMatch(matchID: string) {
    this.match_selected = this.alldata_head_head.find(obj => obj.id === matchID);
    this.home_selected = this.match_selected.data.home;
    this.away_selected = this.match_selected.data.away;
  }
  initData() {
    this.teams_selected = [];
    this.matches_selected = [];
    this.match_selected = {};
    this.home_selected = undefined;
    this.away_selected = undefined;
  }

  getObjectKeys(obj: Record<string, any>): string[] {
    return Object.keys(obj);
  }

  /* Resume by Leagues */
  allDataLeaguesResume() {
    for(const league of this.alldataclassements) {
      
      if(!this.HaFormeLeaguesMap[league.id]) {
        this.HaFormeLeaguesMap[league.id] = [];
      }
      
      if(!this.HomeFormeLeaguesMap[league.id]) {
        this.HomeFormeLeaguesMap[league.id] = [];
      }
      
      if(!this.AwayFormeLeaguesMap[league.id]) {
        this.AwayFormeLeaguesMap[league.id] = [];
      }
      for(const team of league.data) {
        const last_5_ha = team.last_h_a.slice(0,5);
        this.HaFormeLeaguesMap = this.fillForm(team.team, league.id, last_5_ha, this.HaFormeLeaguesMap);
        this.HomeFormeLeaguesMap = this.fillForm(team.team, league.id, team.last5_home, this.HomeFormeLeaguesMap);
        this.AwayFormeLeaguesMap = this.fillForm(team.team, league.id, team.last5_away, this.AwayFormeLeaguesMap);
      }
      this.HaFormeLeaguesMap[league.id].sort((a, b) => b.series.points - a.series.points);
      this.HomeFormeLeaguesMap[league.id].sort((a, b) => b.series.points - a.series.points);
      this.AwayFormeLeaguesMap[league.id].sort((a, b) => b.series.points - a.series.points);
    }
  }

  fillForm(nameTeam: string, league_id: string, lastMatches, mapForm: any) {
    let ha_team = {'name': nameTeam, 'series': {}};
    const last_5 = lastMatches;
    let points = 0;
    let gf_total = 0;
    let ga_total = 0;
    let possession_total = 0;
    let xg_net_for_total = 0;
    let xg_net_against_total = 0;
    let wins = 0;
    for (const last of last_5) {
      if(last.result === 'W') {
        points = points + 3;
        wins = wins + 1;
      }
      else if(last.result === 'D') {
       points = points + 1;
       wins = wins + 0.5;
      }
      gf_total = gf_total + parseFloat(last.GF);
      ga_total = ga_total + parseFloat(last.GA);
      possession_total = possession_total + parseFloat(last.possession);
      xg_net_for_total = xg_net_for_total + +  parseFloat(last.GF) - parseFloat(last.xg_for);
      xg_net_against_total = xg_net_against_total +  parseFloat(last.GA) - parseFloat(last.xg_against);
    }
    const wpc = Number((wins/last_5.length).toFixed(2));
    const pyth = Math.pow(gf_total,2)/(Math.pow(gf_total,2) + Math.pow(ga_total,2));
    ha_team.series= {
    'points': Number((points/5).toFixed(2)), 
    'possession': Number((possession_total/5).toFixed(2)),
    'gf': gf_total,
    'ga': ga_total,
    'xg_net_for': Number(xg_net_for_total.toFixed(2)),
    'xg_net_against': Number(xg_net_against_total.toFixed(2)),
    'wpc': wpc,
    'pyth': Number(pyth.toFixed(2))
    };
    mapForm[league_id].push(ha_team);
    return mapForm;
  }
}
