import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataStatsRoutingModule } from './data-stats-routing.module';
import { DataStatsPage } from './data-stats.page';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CardModule } from 'primeng/card';
import { BadgeModule } from 'primeng/badge';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ClassementcardComponent } from '../components/lotofoot/classementcard/classementcard.component';


@NgModule({
  declarations: [DataStatsPage, ClassementcardComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardModule,
    BadgeModule,
    ToggleButtonModule,
    DataStatsRoutingModule
  ]
})
export class DataStatsModule { }
