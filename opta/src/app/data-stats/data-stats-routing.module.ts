import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataStatsPage } from './data-stats.page';

const routes: Routes = [
  {
    path: '',
    component: DataStatsPage
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataStatsRoutingModule { }
