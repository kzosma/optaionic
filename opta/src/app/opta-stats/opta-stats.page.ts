import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ChartTimingService } from '../service/chart-timing.service';
import { ChartWhoScoredService } from '../service/chart-whoscored.service';
import { ChartFixturesService } from '../service/chart-fixtures.service';
import { ChartClassementService } from '../service/chart-classement-service';

@Component({
  selector: 'app-opta-stats',
  templateUrl: './opta-stats.page.html',
  styleUrls: ['./opta-stats.page.scss'],
})
export class OptaStatsPage implements OnInit {

  home_id: any;
  away_id: any;
  type: any;

  // VARIABLES
  season = 2023

  // SERVICES
  chartTiming: ChartTimingService;
  chartWhoscored: ChartWhoScoredService;
  chartFixture: ChartFixturesService;
  chartClassements: ChartClassementService;

  globalData = {
   'id': 'timing_global',
   'header': 'Timing',
   'data': {},
   'options': {}
  };

  homeDataTiming = {
    'id': '',
    'header': '',
    'data': {},
    'options': {}
   };
   awayDataTiming = {
    'id': '',
    'header': '',
    'data': {},
    'options': {}
   };

   // Distance Datas

   fixturesDistance = {
    'id': 'data_distance',
    'header': 'Distance',
    'data': {},
    'options': {}
   };

   fixturesGoalsForHome = {
    'id': 'data_goals_for_against',
    'header': 'Buts Pour et Contre',
    'data': {},
    'options': {}
   };

   fixturesGoalsForAway = {
    'id': 'data_goals_for_against',
    'header': 'Buts Pour et Contre',
    'data': {},
    'options': {}
   };

   fixturesAttForAgainstHome = {
    'id': 'data_att_def_for_home',
    'header': '',
    'data': {},
    'options': {}
   };

   fixturesAttForAgainstAway = {
    'id': 'data_att_def_for_away',
    'header': '',
    'data': {},
    'options': {}
   };

   /* Radar Team Fixture */

   radarWhoScoredGlobal = {
    'id': 'data_radar_whoscored_global',
    'header': 'Radar WhoScored',
    'data': {
      labels: ['XGChain90', 'XGBuildup90', 'G90', 'Sorare90'],
      datasets: [
          {
              label: '',
              backgroundColor: 'rgba(45,39,245,0.2)',
              borderColor: 'rgba(45,39,245,0.2)',
              pointBackgroundColor: 'rgba(45,39,245,0.2)',
              pointBorderColor: '#fff',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(45,39,245,0.2)',
              data: []
          },
          {
              label: '',
              backgroundColor: 'rgba(255,99,132,0.2)',
              borderColor: 'rgba(255,99,132,1)',
              pointBackgroundColor: 'rgba(255,99,132,1)',
              pointBorderColor: '#fff',
              pointHoverBackgroundColor: '#fff',
              pointHoverBorderColor: 'rgba(255,99,132,1)',
              data: []
          }
      ]
    }
   }



   // WhoScored Global
   data_whoscored_global_home = {
    "goals": 0,
    "shots": 0,
    "yellow_card": 0,
    "red_card": 0,
    "xgChain": 0,
    "xgBuildup": 0,
    "points": 0,
    "rank_def_league": [],
    "rank_mil_league": [],
    "rank_mil_att_league": [],
    "rank_att_league": [],
    "rank_sub_league": [],
    "rank_total_league": []
   };
   data_whoscored_global_away = {
    "goals": 0,
    "shots": 0,
    "yellow_card": 0,
    "red_card": 0,
    "xgChain": 0,
    "xgBuildup": 0,
    "points": 0,
    "rank_def_league": [],
    "rank_mil_league": [],
    "rank_mil_att_league": [],
    "rank_att_league": [],
    "rank_sub_league": [],
    "rank_total_league": []
   };

   // fixtures XGBuildup
   fixturesXGBuildup = {
    'id': 'data_fixtures_xgbuildup',
    'header': 'Fixtures XGBuildup',
    'data': {},
    'options': {}
   };
   // fixtures XGChain
   fixturesXGChain = {
      'id': 'data_fixtures_xgchain',
      'header': 'Fixtures XGchain',
      'data': {},
      'options': {}
    };
    
   // fixtures Delta Goals - XG
   fixturesDeltaGoalsXG = {
    'id': 'data_fixtures_delta_goals_xg',
    'header': 'Fixtures Delta Goals XG',
    'data': {},
    'options': {}
  };

  // fixtures Goals On Shots
  fixturesGoalsOnShots = {
    'id': 'data_fixtures_goals_on_shots',
    'header': 'Fixtures Delta Goals On Shots',
    'data': {},
    'options': {}
  };

  // fixtures Sorare Points
  fixturesSorarePoints = {
    'id': 'data_fixtures_sorare_points',
    'header': 'Fixtures Delta Sorare Points',
    'data': {},
    'options': {}
  };

  fixturesSorarePointsDef = {
    'id': 'data_fixtures_sorare_points_def',
    'header': 'Sorare Points Defense',
    'data': {},
    'options': {}
  };

  fixturesSorarePointsMil = {
    'id': 'data_fixtures_sorare_points_mil',
    'header': 'Sorare Points Milieu',
    'data': {},
    'options': {}
  };
  fixturesSorarePointsAtt = {
    'id': 'data_fixtures_sorare_points_att',
    'header': 'Sorare Points Attaque',
    'data': {},
    'options': {}
  };

  fixturesSorarePointsAttMil = {
    'id': 'data_fixtures_sorare_points_att_mil',
    'header': 'Sorare Points Attaque Milieu',
    'data': {},
    'options': {}
  };

  fixturesSorarePointsSub = {
    'id': 'data_fixtures_sorare_points_sub',
    'header': 'Sorare Points Substitute',
    'data': {},
    'options': {}
  };
  fixturesSorarePointsTotal = {
    'id': 'data_fixtures_sorare_points_total',
    'header': 'Sorare Points Total',
    'data': {},
    'options': {}
  };

  gameStateTime_Home = {
    'id': 'data_gamestate_time_home',
    'header': '',
    'data': {},
    'options': {}
  };

  gameStateTime_Away = {
    'id': 'data_gamestate_time_away',
    'header': '',
    'data': {},
    'options': {}
  };

  gameStateGoals_0_Home = {
    'id': 'data_gamestate_goals_0_home',
    'header': '',
    'data': {},
    'options': {}
  };
  gameStateGoals_More_Home = {
    'id': 'data_gamestate_goals_more_home',
    'header': '',
    'data': {},
    'options': {}
  };
 
  gameStateGoals_0_Away = {
    'id': 'data_gamestate_goals_0_away',
    'header': '',
    'data': {},
    'options': {}
  };
  gameStateGoals_More_Away = {
    'id': 'data_gamestate_goals_more_away',
    'header': '',
    'data': {},
    'options': {}
  };

  gameStateGoals_Less_Home = {
    'id': 'data_gamestate_goals_less_home',
    'header': '',
    'data': {},
    'options': {}
  };

  gameStateGoals_Less_Away = {
    'id': 'data_gamestate_goals_less_away',
    'header': '',
    'data': {},
    'options': {}
  };

  dataTablePlayers = {
    'aggression': [],
    'shotsPerGame': [],
    'assists': [],
    'goals': []
  }

  dataResumeHome =  {
    'title': '',
    'id': '',
    'data': []
  }

  dataResumeAway =  {
    'title': '',
    'id': '',
    'data': []
  }

  lotoFootData = {
    'dataClassementGeneral':{
      'bgcolor': '#231f20',
      'headercolor': '#008bcd',
      'textcolor': '#FFFFFF',
      'subtitle':new Date().toLocaleDateString(),
      'title': 'Classement général',
      'idHome': '',
      'idAway': '',
      'data': [],
      'type': 'general'
    },
    'dataClassementDomicile':{
      'bgcolor': '#231f20',
      'headercolor': '#00a651',
      'textcolor': '#FFFFFF',
      'subtitle': new Date().toLocaleDateString(),
      'title': 'Classement domicile',
      'idHome': '',
      'idAway': '',
      'data': [],
      'type': 'general'
    },
    'dataClassementExterieur':{
      'bgcolor': '#231f20',
      'headercolor': '#fff100',
      'textcolor': '#FFFFFF',
      'subtitle': new Date().toLocaleDateString(),
      'title': 'Classement extérieur',
      'idHome': '',
      'idAway': '',
      'data': [],
      'type': 'general'
    },
    'dataClassementHandicap': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'subtitle': new Date().toLocaleDateString(),
      'title': 'Classement Handicap',
      'idHome': '',
      'idAway': '',
      'data': [],
      'type': 'handicap'
    },
    'dataClassementPythagoreanExpectation':{
      'bgcolor': '#231f20',
      'headercolor': '#008bcd',
      'textcolor': '#FFFFFF',
      'subtitle':new Date().toLocaleDateString(),
      'title': 'Classement Pythagorean Expectation',
      'idHome': '',
      'idAway': '',
      'data': [],
      'type': 'pythexpect'
    },
    'dataLastMatchHome': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'subtitle': new Date().toLocaleDateString(),
      'title': '',
      'data': []
    },
    'dataLastMatchAway': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'subtitle': new Date().toLocaleDateString(),
      'title': '',
      'data': []
    },
    'dataBestScorerHome': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'highlightcolor': '#ea8a26',
      'subtitle': new Date().toLocaleDateString(),
      'title': '',
      'data': []
    },
    'dataBestScorerAway': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'highlightcolor': '#008bcd',
      'subtitle': new Date().toLocaleDateString(),
      'title': '',
      'data': []
    },
    'dataKNearestHome': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'highlightcolor': '#ea8a26',
      'subtitle': new Date().toLocaleDateString(),
      'title': '',
      'data': []
    },
    'dataKNearestAway': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'highlightcolor': '#008bcd',
      'subtitle': new Date().toLocaleDateString(),
      'title': '',
      'data': []
    },


    'dataBestAssistHome': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'highlightcolor': '#ea8a26',
      'subtitle': new Date().toLocaleDateString(),
      'title': '',
      'data': []
    },
    'dataBestAssistAway': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'highlightcolor': '#008bcd',
      'subtitle': new Date().toLocaleDateString(),
      'title': '',
      'data': []
    },
    'dataSyntheseHome': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'highlightcolor': '#ea8a26',
      'subtitle': new Date().toLocaleDateString(),
      'title': '',
      'data': []
    },
    'dataSyntheseAway': {
      'bgcolor': '#231f20',
      'headercolor': '#fbe4ca',
      'textcolor': '#FFFFFF',
      'highlightcolor': '#008bcd',
      'subtitle': new Date().toLocaleDateString(),
      'title': '',
      'data': []
    }
  };
 
  constructor(
     private activeRoute: ActivatedRoute,
     private chart_timing: ChartTimingService,
     private chart_fixtures: ChartFixturesService,
     private chart_whoscored: ChartWhoScoredService,
     private chart_classements: ChartClassementService) {
      this.chartTiming = chart_timing;
      this.chartWhoscored = chart_whoscored;
      this.chartFixture = chart_fixtures;
      this.chartClassements = chart_classements;
    }
   
  ngOnInit() {
    this.home_id = this.activeRoute.snapshot.paramMap.get('home');
    this.away_id = this.activeRoute.snapshot.paramMap.get('away');
    this.type = this.activeRoute.snapshot.paramMap.get('type');
    Promise.all([this.chartTiming.processStatsTeam(this.home_id, this.season), this.chartTiming.processStatsTeam(this.away_id, this.season)])
    .then(([home, away]) => {
      let season_str = ''+this.season;
      Promise.all([this.chartTiming.processResumeTeam('', season_str,home.league.toLowerCase()), this.chartTiming.processResumeTeam('', season_str, away.league.toLowerCase())])
      .then(([homeResume, awayResume]) => {
        this.dataResumeHome.id = this.home_id;
        this.dataResumeAway.id = this.away_id;
        this.dataResumeHome.title = home.title;
        this.dataResumeAway.title = away.title;
        this.dataResumeHome.data = homeResume;
        this.dataResumeAway.data = awayResume;
      });
      this.buildDataGlobalTiming(home, away);
      this.buildTeamDataTiming(home, away);
      this.buildDataGlobalFixture(home,away);
      this.buildGameState(home, away);
      this.buildPlayerDataTable(home,away);
    });
    Promise.all([
      this.chartWhoscored.processStatsWhoScored(this.home_id,this.season),
      this.chartWhoscored.processStatsWhoScored(this.away_id,this.season)])
    .then(([home, away]) => {
      this.buildTeamWhoScoredDataGlobal(home.global, away.global);
      this.buildRadarWhoScoredGlobal(home, away);
      this.buildFixturesSorarePoints(home,away);
      if(home.nameLeague === away.nameLeague) {
        this.buildClassementLotoFoot(home.nameLeague, away.nameLeague, this.home_id, this.away_id);
      }
    });
   }
  // Construction modèle de données Data Timing
  buildDataGlobalTiming = (home: any, away: any) => {
    let lastMatchHome = home.fixtures[home.fixtures.length - 1];
    let lastMatchAway = away.fixtures[away.fixtures.length - 1];
    const response = this.chartTiming.buildDataGlobalTiming(home, away);
    this.globalData.data = response.data;
    this.globalData.options = response.options;
  }
  buildTeamDataTiming = (home: any, away: any) => {
    const responseHome = this.chartTiming.buildDataTimingTeam(home, 'home_data_timing', home.title);
    const responseAway = this.chartTiming.buildDataTimingTeam(away, 'away_data_timing', away.title);
    // HOME
    this.homeDataTiming.data = responseHome.data;
    this.homeDataTiming.options = responseHome.options;
    this.homeDataTiming.id = responseHome.id;
    this.homeDataTiming.header = responseHome.header;
    // AWAY
    this.awayDataTiming.data = responseAway.data;
    this.awayDataTiming.options = responseAway.options;
    this.awayDataTiming.id = responseAway.id;
    this.awayDataTiming.header = responseAway.header;
  }
  buildTeamWhoScoredDataGlobal = (home: any, away: any) => {
    // HOME
    this.data_whoscored_global_home.goals = home.goals;
    this.data_whoscored_global_home.shots = home.shots;
    this.data_whoscored_global_home.yellow_card = home.yellow_card;
    this.data_whoscored_global_home.red_card = home.red_card;
    this.data_whoscored_global_home.xgChain = Number(home.xgChain.toFixed(2));
    this.data_whoscored_global_home.xgBuildup = Number(home.xgBuildup.toFixed(2));
    this.data_whoscored_global_home.points = Number((+home.points).toFixed(2));
    this.data_whoscored_global_home.rank_def_league = home.rank_def_league.map(a => a.nameTeam);
    this.data_whoscored_global_home.rank_mil_att_league = home.rank_att_mil_league.map(a => a.nameTeam);
    this.data_whoscored_global_home.rank_mil_league = home.rank_mil_league.map(a => a.nameTeam);
    this.data_whoscored_global_home.rank_att_league = home.rank_att_league.map(a => a.nameTeam);
    this.data_whoscored_global_home.rank_sub_league = home.rank_sub_league.map(a => a.nameTeam);
    this.data_whoscored_global_home.rank_total_league = home.rank_total_league.map(a => a.nameTeam);
    // AWAY
    this.data_whoscored_global_away.goals = away.goals;
    this.data_whoscored_global_away.shots = away.shots;
    this.data_whoscored_global_away.yellow_card = away.yellow_card;
    this.data_whoscored_global_away.red_card = away.red_card;
    this.data_whoscored_global_away.xgChain = Number(away.xgChain.toFixed(2));
    this.data_whoscored_global_away.xgBuildup = Number(away.xgBuildup.toFixed(2));
    this.data_whoscored_global_away.points = Number((+away.points).toFixed(2));
    
    this.data_whoscored_global_away.rank_def_league = away.rank_def_league.map(a => a.nameTeam);
    this.data_whoscored_global_away.rank_mil_att_league = away.rank_att_mil_league.map(a => a.nameTeam);
    this.data_whoscored_global_away.rank_mil_league = away.rank_mil_league.map(a => a.nameTeam);
    this.data_whoscored_global_away.rank_att_league = away.rank_att_league.map(a => a.nameTeam);
    this.data_whoscored_global_away.rank_sub_league = away.rank_sub_league.map(a => a.nameTeam);
    this.data_whoscored_global_away.rank_total_league = away.rank_total_league.map(a => a.nameTeam);
  }
  // Construction modèle de données Data Fixture
  buildDataGlobalFixture = (home: any, away: any) => {
    const response = this.chartFixture.buildDataGlobalFixtures(home, away);
    const responseGoalsForHome = this.chartFixture.buildDataGlobalFixturesGoalFor(home);
    const responseGoalsForAway = this.chartFixture.buildDataGlobalFixturesGoalFor(away);
    
    const responseAttDefHome = this.chartFixture.buildDataGlobalFixturesAttDefTeam(home);
    const responseAttDefAway = this.chartFixture.buildDataGlobalFixturesAttDefTeam(away);
    this.fixturesDistance.data = response.data;
    this.fixturesDistance.options = response.options;
    
    this.fixturesGoalsForHome.data = responseGoalsForHome.data;
    this.fixturesGoalsForHome.options = responseGoalsForHome.options;
    this.fixturesGoalsForHome.header = home.title + ": " + this.fixturesGoalsForHome.header; 

    this.fixturesGoalsForAway.data = responseGoalsForAway.data;
    this.fixturesGoalsForAway.options = responseGoalsForAway.options;
    this.fixturesGoalsForAway.header = away.title + ": " + this.fixturesGoalsForAway.header; 
    

    this.fixturesAttForAgainstHome.data = responseAttDefHome.data;
    this.fixturesAttForAgainstHome.options = responseAttDefHome.options;
    this.fixturesAttForAgainstHome.header = home.title + ': Attaque Pour et Contre ';

    this.fixturesAttForAgainstAway.data = responseAttDefAway.data;
    this.fixturesAttForAgainstAway.options = responseAttDefAway.options;
    this.fixturesAttForAgainstAway.header = away.title + ': Attaque Pour et Contre';
  }

  buildFixturesSorarePoints = (home: any, away: any) => {
    const responseDef = this.chartFixture.buildFixturesSorarePoints(home, away, "Sorare Point Defense", "data_sorare_points_def", "def");
    this.fixturesSorarePointsDef.data = responseDef.data;
    this.fixturesSorarePointsDef.options = responseDef.options;
    const responseMil = this.chartFixture.buildFixturesSorarePoints(home, away, "Sorare Point Milieu", "data_sorare_points_mil", "mil");
    this.fixturesSorarePointsMil.data = responseMil.data;
    this.fixturesSorarePointsMil.options = responseMil.options;
    const responseAtt = this.chartFixture.buildFixturesSorarePoints(home, away, "Sorare Point Attaque", "data_sorare_points_att", "att");
    this.fixturesSorarePointsAtt.data = responseAtt.data;
    this.fixturesSorarePointsAtt.options = responseAtt.options;
    const responseAttMil = this.chartFixture.buildFixturesSorarePoints(home, away, "Sorare Point Attaque Milieu", "data_sorare_points_att_mil", "att_mil");
    this.fixturesSorarePointsAttMil.data = responseAttMil.data;
    this.fixturesSorarePointsAttMil.options = responseAttMil.options;
    const responseSub = this.chartFixture.buildFixturesSorarePoints(home, away, "Sorare Point Substitute", "data_sorare_points_sub", "sub");
    this.fixturesSorarePointsSub.data = responseSub.data;
    this.fixturesSorarePointsSub.options = responseSub.options;
    const responseTotal = this.chartFixture.buildFixturesSorarePoints(home, away, "Sorare Point Total", "data_sorare_points_total", "total");
    this.fixturesSorarePointsTotal.data = responseTotal.data;
    this.fixturesSorarePointsTotal.options = responseTotal.options;

  }

  // Build Radar WhoScored Global
  buildRadarWhoScoredGlobal = (home: any, away: any) => {
    // HOME
    this.radarWhoScoredGlobal.data.datasets[0].label = home.nameTeam + '( '+home.rank +')';
    this.radarWhoScoredGlobal.data.datasets[0].data.push(this.calculateStatRadar(home.global.xgChain, 1,2));
    this.radarWhoScoredGlobal.data.datasets[0].data.push(this.calculateStatRadar(home.global.xgBuildup, 1,2));
    this.radarWhoScoredGlobal.data.datasets[0].data.push(this.calculateStatRadar(home.global.goals, 1,2));
    this.radarWhoScoredGlobal.data.datasets[0].data.push(this.calculateStatRadar(home.global.points, home.fixtures.length,2));
    
    // AWAY
    this.radarWhoScoredGlobal.data.datasets[1].label = away.nameTeam + '( '+away.rank +')';
    this.radarWhoScoredGlobal.data.datasets[1].data.push(this.calculateStatRadar(away.global.xgChain, 1,2));
    this.radarWhoScoredGlobal.data.datasets[1].data.push(this.calculateStatRadar(away.global.xgBuildup, 1,2));
    this.radarWhoScoredGlobal.data.datasets[1].data.push(this.calculateStatRadar(away.global.goals, 1,2));
    this.radarWhoScoredGlobal.data.datasets[1].data.push(this.calculateStatRadar(away.global.points, away.fixtures.length,2));
  }
  buildGameState = (home: any, away: any) => {

    // TIME
    const responseGameStateTime_Home = this.chartFixture.buildGameStateTime(home, "Game State Time " + home.title, "data_game_state_time_"+home.title, "gamestate");
    this.gameStateTime_Home.data = responseGameStateTime_Home.data;
    this.gameStateTime_Home.options = responseGameStateTime_Home.options;
    this.gameStateTime_Home.header = responseGameStateTime_Home.header;

    const responseGameStateTime_Away = this.chartFixture.buildGameStateTime(away, "Game State Time " + away.title, "data_game_state_time_"+away.title, "gamestate");
    this.gameStateTime_Away.data = responseGameStateTime_Away.data;
    this.gameStateTime_Away.options = responseGameStateTime_Away.options;
    this.gameStateTime_Away.header = responseGameStateTime_Away.header;

    // GOALS
    const responseGameStateGoals_Home_0 = this.chartFixture.buildGameStateGoals(home, "Game State Goals 0 " + home.title, "data_game_state_goals_"+home.title, "0");
    this.gameStateGoals_0_Home.data = responseGameStateGoals_Home_0.data;
    this.gameStateGoals_0_Home.options = responseGameStateGoals_Home_0.options;
    this.gameStateGoals_0_Home.header = responseGameStateGoals_Home_0.header;

    
    const responseGameStateGoals_Away_0 = this.chartFixture.buildGameStateGoals(away, "Game State Goals 0 " + away.title, "data_game_state_goals_"+away.title, "0");
    this.gameStateGoals_0_Away.data = responseGameStateGoals_Away_0.data;
    this.gameStateGoals_0_Away.options = responseGameStateGoals_Away_0.options;
    this.gameStateGoals_0_Away.header = responseGameStateGoals_Away_0.header;
    

    const responseGameStateGoals_Home_More = this.chartFixture.buildGameStateGoals(home, "Game State Goals More " + home.title, "data_game_state_goals_"+home.title, "more");
    this.gameStateGoals_More_Home.data = responseGameStateGoals_Home_More.data;
    this.gameStateGoals_More_Home.options = responseGameStateGoals_Home_More.options;
    this.gameStateGoals_More_Home.header = responseGameStateGoals_Home_More.header;

    const responseGameStateGoals_Away_More = this.chartFixture.buildGameStateGoals(away, "Game State Goals More " + away.title, "data_game_state_goals_"+away.title, "more");
    this.gameStateGoals_More_Away.data = responseGameStateGoals_Away_More.data;
    this.gameStateGoals_More_Away.options = responseGameStateGoals_Away_More.options;
    this.gameStateGoals_More_Away.header = responseGameStateGoals_Away_More.header;

    const responseGameStateGoals_Home_Less = this.chartFixture.buildGameStateGoals(home, "Game State Goals Less " + home.title, "data_game_state_goals_less_"+home.title, "less");
    this.gameStateGoals_Less_Home.data = responseGameStateGoals_Home_Less.data;
    this.gameStateGoals_Less_Home.options = responseGameStateGoals_Home_Less.options;
    this.gameStateGoals_Less_Home.header = responseGameStateGoals_Home_Less.header;

    const responseGameStateGoals_Away_Less = this.chartFixture.buildGameStateGoals(away, "Game State Goals Less " + away.title, "data_game_state_goals_less_"+away.title, "less");
    this.gameStateGoals_Less_Away.data = responseGameStateGoals_Away_Less.data;
    this.gameStateGoals_Less_Away.options = responseGameStateGoals_Away_Less.options;
    this.gameStateGoals_Less_Away.header = responseGameStateGoals_Away_Less.header;
  }
  buildPlayerDataTable = (home: any, away: any) => {

    // Aggression
    let mapAggression: { [id: string]: any; } = {};
    mapAggression = this.calculateAgression(home, mapAggression);
    mapAggression = this.calculateAgression(away, mapAggression);
    Object.keys(mapAggression).forEach(p => {
      mapAggression[p].rank = Number(mapAggression[p].yellow) + 3 * mapAggression[p].red;
      this.dataTablePlayers.aggression.push(mapAggression[p]);
    });
    this.dataTablePlayers.aggression.sort((a, b) => (b.rank < a.rank ? -1 : 1));
    this.dataTablePlayers.aggression = this.dataTablePlayers.aggression.slice(0,5);
    
    // Shots Per Game
    let mapShotsPerGame: { [id: string]: any; } = {};
    mapShotsPerGame = this.calculateShotsPerGame(home, mapShotsPerGame);
    mapShotsPerGame = this.calculateShotsPerGame(away, mapShotsPerGame);
    Object.keys(mapShotsPerGame).forEach(p => {
      this.dataTablePlayers.shotsPerGame.push(mapShotsPerGame[p]);
    });
    this.dataTablePlayers.shotsPerGame.sort((a, b) => (b.shots_per_game < a.shots_per_game ? -1 : 1));
    this.dataTablePlayers.shotsPerGame = this.dataTablePlayers.shotsPerGame.slice(0,5);

    // Assists
    let mapAssists: { [id: string]: any; } = {};
    mapAssists = this.calculateAssists(home, mapAssists);
    mapAssists = this.calculateAssists(away, mapAssists);
    Object.keys(mapAssists).forEach(p => {
      this.dataTablePlayers.assists.push(mapAssists[p]);
    });
    this.dataTablePlayers.assists.sort((a, b) => (b.assist < a.assist ? -1 : 1));
    this.dataTablePlayers.assists = this.dataTablePlayers.assists.slice(0,5);

    // Goals
    let mapGoals: { [id: string]: any; } = {};
    mapGoals = this.calculateGoals(home, mapGoals);
    mapGoals = this.calculateGoals(away, mapGoals);
    Object.keys(mapGoals).forEach(p => {
      this.dataTablePlayers.goals.push(mapGoals[p]);
    });
    this.dataTablePlayers.goals.sort((a, b) => (b.goals < a.goals ? -1 : 1));
    this.dataTablePlayers.goals = this.dataTablePlayers.goals.slice(0,5);
  }

  buildClassementLotoFoot = (home_league: string, awayLeague:string, idHome: string, idAway: string) => {
    const classement_general = 'Rank_League_'+home_league;
    const classement_handicap = 'Rank_Handicap_'+home_league;
    this.chartClassements.processStatsClassements(classement_general,this.season).then((res) => {

      // Classement Pythagorean Expectation
      const classement_pyth_exp = Object.assign([], res.list).sort((a,b) => {return b.pyth_exp - a.pyth_exp}); 
      const classement_wpc_exp: [] = Object.assign([], res.list).sort((a,b) => {return b.wpc_exp - a.wpc_exp});
      const classement_wpc_calc: [] = Object.assign([], res.list).sort((a,b) => {return b.wpc_calc - a.wpc_calc}); 
      
      // Derniers matchs
      const homeFilter = res.list.filter((e) => e.idTeam === idHome)[0];
      const awayFilter = res.list.filter((e) => e.idTeam === idAway)[0];
      const lastMatchsHome = homeFilter.last_matchs;
      const lastMatchsAway = awayFilter.last_matchs;
      lastMatchsHome.reverse().forEach((t,i) => {
        this.lotoFootData.dataLastMatchHome.data.push(this.buildLastMatch(t,i));
      });
      this.lotoFootData.dataLastMatchHome.title = 'Derniers matchs de ' + homeFilter.team;
      lastMatchsAway.reverse().forEach((t,i) => {
        this.lotoFootData.dataLastMatchAway.data.push(this.buildLastMatch(t,i));
      });
      this.lotoFootData.dataLastMatchAway.title = 'Derniers matchs de ' + awayFilter.team;
      
      homeFilter['3_best_scorer'].forEach((t,i) => {
        this.lotoFootData.dataBestScorerHome.data.push(this.buildBestScorer(t,i));
      });
      this.lotoFootData.dataBestScorerHome.title = 'Meilleurs buteurs de  ' + homeFilter.team;
      
      awayFilter['3_best_scorer'].forEach((t,i) => {
        this.lotoFootData.dataBestScorerAway.data.push(this.buildBestScorer(t,i));
      });
      this.lotoFootData.dataBestScorerAway.title = 'Meilleurs buteurs de  ' + awayFilter.team;
      
      homeFilter['3_best_assists'].forEach((t,i) => {
        this.lotoFootData.dataBestAssistHome.data.push(this.buildBestScorer(t,i));
      });
      this.lotoFootData.dataBestAssistHome.title = 'Meilleurs passeurs de  ' + homeFilter.team;
      
      awayFilter['3_best_assists'].forEach((t,i) => {
        this.lotoFootData.dataBestAssistAway.data.push(this.buildBestScorer(t,i));
      });
      this.lotoFootData.dataBestAssistAway.title = 'Meilleurs passeurs de  ' + awayFilter.team;
      
      homeFilter['k_nearest_national_teams']['total'].forEach((t,i) => {
        this.lotoFootData.dataKNearestHome.data.push(this.buildKNearest(t,i));
      });
      this.lotoFootData.dataKNearestHome.title = 'Equipes proche de  ' + homeFilter.team;
      
      awayFilter['k_nearest_national_teams']['total'].forEach((t,i) => {
         this.lotoFootData.dataKNearestAway.data.push(this.buildKNearest(t,i));
      });
      this.lotoFootData.dataKNearestAway.title = 'Equipe proche  de  ' + awayFilter.team;
      
      this.lotoFootData.dataSyntheseHome.data.push({'rank': '1 - 5', 'G': homeFilter['synthese_1_5']['G'], 'N': homeFilter['synthese_1_5']['N'], 'P': homeFilter['synthese_1_5']['P'], 'BP': homeFilter['synthese_1_5']['BP'], 'BC':homeFilter['synthese_1_5']['BC']});
      this.lotoFootData.dataSyntheseHome.data.push({'rank': '5 - 10', 'G': homeFilter['synthese_5_10']['G'], 'N': homeFilter['synthese_5_10']['N'], 'P': homeFilter['synthese_5_10']['P'], 'BP': homeFilter['synthese_5_10']['BP'], 'BC':homeFilter['synthese_5_10']['BC']});
      this.lotoFootData.dataSyntheseHome.data.push({'rank': '10 - End', 'G': homeFilter['synthese_10_end']['G'], 'N': homeFilter['synthese_10_end']['N'], 'P': homeFilter['synthese_10_end']['P'], 'BP': homeFilter['synthese_10_end']['BP'], 'BC':homeFilter['synthese_10_end']['BC']});
      this.lotoFootData.dataSyntheseHome.title = 'Synthèse par Classement pour ' + homeFilter.team;
     
      this.lotoFootData.dataSyntheseAway.data.push({'rank': '1 - 5', 'G': awayFilter['synthese_1_5']['G'], 'N': awayFilter['synthese_1_5']['N'], 'P': awayFilter['synthese_1_5']['P'], 'BP': awayFilter['synthese_1_5']['BP'], 'BC':awayFilter['synthese_1_5']['BC']});
      this.lotoFootData.dataSyntheseAway.data.push({'rank': '5 - 10', 'G': awayFilter['synthese_5_10']['G'], 'N': awayFilter['synthese_5_10']['N'], 'P': awayFilter['synthese_5_10']['P'], 'BP': awayFilter['synthese_5_10']['BP'], 'BC':awayFilter['synthese_5_10']['BC']});
      this.lotoFootData.dataSyntheseAway.data.push({'rank': '10 - End', 'G': awayFilter['synthese_10_end']['G'], 'N': awayFilter['synthese_10_end']['N'], 'P': awayFilter['synthese_10_end']['P'], 'BP': awayFilter['synthese_10_end']['BP'], 'BC':awayFilter['synthese_10_end']['BC']});
      this.lotoFootData.dataSyntheseAway.title = 'Synthèse par Classement pour ' + awayFilter.team;
      
            // Classement Pythagorean Expectation
    classement_pyth_exp.forEach((t,i) => {
      const wpc_exp = classement_wpc_exp.findIndex((e: any)=> e.idTeam === t.idTeam) + 1;
      const wpc_calc =  classement_wpc_calc.findIndex((e: any)=> e.idTeam === t.idTeam) + 1;
      const equipe =  {
      'index': i + 1,
      'idTeam': t.idTeam,
      'team': t.team,
      'Pts': t.Pts,
      'G': t.G,
      'N': t.N,
      'P': t.P,
      'Diff': t.Diff,
      'pyth_exp': t.pyth_exp,
      'wpc_exp': wpc_exp,
      'wpc_calc': wpc_calc,
      'evol': wpc_exp - wpc_calc
    }
      this.lotoFootData.dataClassementPythagoreanExpectation.idHome = idHome;
      this.lotoFootData.dataClassementPythagoreanExpectation.idAway = idAway;
      this.lotoFootData.dataClassementPythagoreanExpectation.data.push(equipe);
    });
      // console.log(this.lotoFootData.dataClassementPythagoreanExpectation.data);
  
      // Classement général
      this.lotoFootData.dataClassementGeneral.idHome = idHome;
      this.lotoFootData.dataClassementGeneral.idAway = idAway;
      res.list.forEach((t,i) => {
        const equipe =  {
          'index': i + 1,
          'idTeam': t.idTeam,
          'team': t.team,
          'Pts': t.Pts,
          'G': t.G,
          'N': t.N,
          'P': t.P,
          'Diff': t.Diff
        }
        this.lotoFootData.dataClassementGeneral.data.push(equipe);
        });
      // Classement domicile
      res.list.sort((a, b) => (b.Pts_Dom < a.Pts_Dom ? -1 : 1));
      res.list.forEach((t,i) => {
        const equipe =  {'index': i + 1,'idTeam': t.idTeam,'team': t.team,'Pts': t.Pts_Dom,'G': t.G_Dom,'N': t.N_Dom,'P': t.P_Dom,'Diff': t.Diff_Dom}
        this.lotoFootData.dataClassementDomicile.idHome = idHome;
        this.lotoFootData.dataClassementDomicile.idAway = idAway;
        this.lotoFootData.dataClassementDomicile.data.push(equipe);
      });
      // Classement extérieure
      res.list.sort((a, b) => (b.Pts_Ext < a.Pts_Ext ? -1 : 1));
      res.list.forEach((t,i) => {
        const equipe =  {'index': i + 1,'idTeam': t.idTeam,'team': t.team,'Pts': t.Pts_Ext,'G': t.G_Ext,'N': t.N_Ext,'P': t.P_Ext,'Diff': t.Diff_Ext}
        this.lotoFootData.dataClassementExterieur.idHome = idHome;
        this.lotoFootData.dataClassementExterieur.idAway = idAway;
        this.lotoFootData.dataClassementExterieur.data.push(equipe);
      });
    });
    // Classement handicap
    this.delay(1000).then(() => {
      this.chartClassements.processStatsClassements(classement_handicap,this.season).then((res) => {
        res.list.forEach((t,i) => {
          const equipe =  {'index': i + 1,'idTeam': t.idTeam,'team': t.team,'V2': t['V>2'], 'V1': t['V2'], 'V0': t['V1'], 'D2': t['D>2'], 'D1': t['D2'], 'D0': t['D1']}
          this.lotoFootData.dataClassementHandicap.idHome = idHome;
          this.lotoFootData.dataClassementHandicap.idAway = idAway;
          this.lotoFootData.dataClassementHandicap.data.push(equipe);
          
        });
      });
    });
  }


  buildLastMatch = (t: any, i: number) => {
    const lastMatch =  {
      'index': i + 1,
      'against_name': t.against_name,
      'against_rank': t.against_rank,
      'own_gf_mt': t.h_a === 'h' ? t.home_gfor_mt: t.away_gfor_mt,
      'own_gf_ft': t.h_a === 'h' ? t.home_gfor_ft: t.away_gfor_ft,
      'against_gf_mt': t.h_a === 'h' ? t.away_gfor_mt: t.home_gfor_mt,
      'against_gf_ft': t.h_a === 'h' ? t.away_gfor_ft: t.home_gfor_ft,
      'own_big_chance': t.h_a === 'h' ? t.home_bigchance: t.away_bigchance,
      'own_big_chance_missed': t.h_a === 'h' ? t.home_bigchance_missed: t.away_bigchance_missed
    }
    return lastMatch;
  }
  buildBestScorer = (t: any, i: number) => {
    const bestScorer =  {
      'index': i + 1,
      'player_name': t.name,
      'gh': t.goal_home,
      'ga': t.goal_away,
      'gt': t.goal_total,
      'ah': t.assist_home,
      'aa': t.assist_away,
      'at': t.assist_total,
      'xgb': Number(t.xgbuildup_total.toFixed(2)),
      'xgc': Number(t.xgchain_total.toFixed(2))
     }
    return bestScorer;
  }

  // K Nearest
  buildKNearest = (t: any, i: number) => {
    const team_name = t.name + ' ( '+ t.id + ' ) ';
    const knearest =  {
      'index': i + 1,
      'team_name': team_name,
      'rank': t.rank,
      'Pts': t.Pts,
      'BP': t.BP,
      'BC': t.BC,
     }
    return knearest;
  }

  // Aggression
  calculateAgression = (team: any, map: any): any => {
    team.fixtures.forEach(fixture => {
      const h_a = fixture.h_a;
      let rosterMatchs = [];
      if(h_a === 'h') {
        rosterMatchs = fixture.rosterMatch.h;
      }
      else if(h_a === 'a') {
        rosterMatchs = fixture.rosterMatch.a;
      }
      Object.entries(rosterMatchs).forEach(([key, value]) => {
        let player_aggression_line = {
          'player_name': '',
          'player_team':'',
          'yellow': 0,
          'red': 0
          };
        if(map[rosterMatchs[key].player]=== undefined) {
          player_aggression_line = {
            'player_name': rosterMatchs[key].player,
            'player_team':team.title,
            'yellow': +rosterMatchs[key].yellow_card,
            'red': +rosterMatchs[key].red_card
            };
        }
        else {
          player_aggression_line = map[rosterMatchs[key].player]; 
          player_aggression_line.yellow = player_aggression_line.yellow + Number(rosterMatchs[key].yellow_card);
          player_aggression_line.red = player_aggression_line.red + Number(rosterMatchs[key].red_card); 
        }
        map[rosterMatchs[key].player] = player_aggression_line;
      });
    });
    return map;
  };

  // Shots Per Game
  calculateShotsPerGame = (team: any, map: any): any => {
    team.fixtures.forEach(fixture => {
      const h_a = fixture.h_a;
      let rosterMatchs = [];
      if(h_a === 'h') {
        rosterMatchs = fixture.rosterMatch.h;
      }
      else if(h_a === 'a') {
        rosterMatchs = fixture.rosterMatch.a;
      }
      let index = 1;
      Object.entries(rosterMatchs).forEach(([key, value]) => {
        let player_shots_per_game_line = {
          'player_name': '',
          'player_team':'',
          'shots_per_game': 0,
          'goals': 0
          };
        if(map[rosterMatchs[key].player]=== undefined) {
          player_shots_per_game_line = {
            'player_name': rosterMatchs[key].player,
            'player_team':team.title,
            'shots_per_game': Number(rosterMatchs[key].shots),
            'goals': Number(rosterMatchs[key].goals)
            };
        }
        else {
          player_shots_per_game_line = map[rosterMatchs[key].player]; 
          player_shots_per_game_line.shots_per_game = this.calculateStatFormatter((((index-1) * player_shots_per_game_line.shots_per_game) + Number(rosterMatchs[key].shots)), index,2);
          player_shots_per_game_line.goals = player_shots_per_game_line.goals + Number(rosterMatchs[key].goals);
        }
        map[rosterMatchs[key].player] = player_shots_per_game_line;
        index +=1;
      });
    });
    return map;
  };

   // Assists
   calculateAssists = (team: any, map: any): any => {
    team.fixtures.forEach(fixture => {
      const h_a = fixture.h_a;
      let rosterMatchs = [];
      if(h_a === 'h') {
        rosterMatchs = fixture.rosterMatch.h;
      }
      else if(h_a === 'a') {
        rosterMatchs = fixture.rosterMatch.a;
      }
      Object.entries(rosterMatchs).forEach(([key, value]) => {
        let player_assists_line = {
          'player_name': '',
          'player_team':'',
          'assist': 0,
          'xgchain': 0
          };
        if(map[rosterMatchs[key].player]=== undefined) {
          player_assists_line = {
            'player_name': rosterMatchs[key].player,
            'player_team':team.title,
            'assist': Number(rosterMatchs[key].assists),
            'xgchain': this.calculateStatFormatter(Number(rosterMatchs[key].xGChain),1,2)
            };
        }
        else {
          player_assists_line = map[rosterMatchs[key].player]; 
          player_assists_line.assist = player_assists_line.assist + Number(rosterMatchs[key].assists);
          player_assists_line.xgchain = this.calculateStatFormatter(player_assists_line.xgchain + Number(rosterMatchs[key].xGChain), 1,2);
        }
        map[rosterMatchs[key].player] = player_assists_line;
      });
    });
    return map;
  };

  // Goals
  calculateGoals = (team: any, map: any): any => {
    team.fixtures.forEach(fixture => {
      const h_a = fixture.h_a;
      let rosterMatchs = [];
      if(h_a === 'h') {
        rosterMatchs = fixture.rosterMatch.h;
      }
      else if(h_a === 'a') {
        rosterMatchs = fixture.rosterMatch.a;
      }
      Object.entries(rosterMatchs).forEach(([key, value]) => {
        let player_goals_line = {
          'player_name': '',
          'player_team':'',
          'goals': 0,
          'xgchain': 0
          };
        if(map[rosterMatchs[key].player]=== undefined) {
          player_goals_line = {
            'player_name': rosterMatchs[key].player,
            'player_team':team.title,
            'goals': Number(rosterMatchs[key].goals),
            'xgchain': this.calculateStatFormatter(Number(rosterMatchs[key].xGChain),1,2)
            };
        }
        else {
          player_goals_line = map[rosterMatchs[key].player]; 
          player_goals_line.goals = player_goals_line.goals + Number(rosterMatchs[key].goals);
          player_goals_line.xgchain = this.calculateStatFormatter(player_goals_line.xgchain + Number(rosterMatchs[key].xGChain), 1,2);
        }
        map[rosterMatchs[key].player] = player_goals_line;
      });
    });
    return map;
  };

  calculateStatRadar = (num: any, den: any, fractionDigits: number): number => {
    if(typeof num === 'string') {
      return Number(((+num)/den).toFixed(fractionDigits));
    }
    else if(typeof num === 'number') {
      return Number((num/den).toFixed(fractionDigits));
    }
    else {
      return 0;
    }
  }

  calculateStatFormatter = (num: any, den: any, fractionDigits: number): number => {
    if(typeof num === 'string') {
      return Number(((+num)/den).toFixed(fractionDigits));
    }
    else if(typeof num === 'number') {
      return Number((num/den).toFixed(fractionDigits));
    }
    else {
      return 0;
    }
  }
  delay = (milliseconds : number) => {
    return new Promise(resolve => setTimeout( resolve, milliseconds));
}
  
}
