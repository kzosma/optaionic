import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { OptaStatsPageRoutingModule } from './opta-stats-routing.module';
import { OptaStatsPage } from './opta-stats.page';
import { BadgeModule} from 'primeng/badge';
import {KnobModule} from 'primeng/knob';
import {TimelineModule} from 'primeng/timeline';
import {CardModule} from 'primeng/card';
import {ChartModule} from 'primeng/chart';

import { TimingStatComponent } from '../components/charts/timing-stat/timing-stat.component';
import { FixturesStatComponent } from '../components/charts/fixtures-stat/fixtures-stat.component';
import { RadarTeamStatComponent } from '../components/charts/radar-team-stat/radar-team-stat.component';
import { WhoscoredGlobalModule } from '../components/charts/whoscored/whoscored-global.module';
import { TablePlayersComponent } from '../components/charts/table-players/table-players.component';
import { FicheteamComponent } from '../components/ficheteam/ficheteam.component';
import { InfocardComponent } from '../components/lotofoot/infocard/infocard.component';
import { ClassementcardComponent } from '../components/lotofoot/classementcard/classementcard.component';
import { LastmatchescardComponent } from '../components/lotofoot/lastmatchescard/lastmatchescard.component';
import { BestscorercardComponent } from '../components/lotofoot/bestscorercard/bestscorercard.component';
import { BestassistcardComponent } from '../components/lotofoot/bestassistcard/bestassistcard.component';
import { SyntheserankcardComponent } from '../components/lotofoot/syntheserankcard/syntheserankcard.component';
import { KnearestcardComponent } from '../components/lotofoot/knearestcard/knearestcard.component';
import { ResumeStatComponent } from '../components/charts/resume-stat/resume-stat.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OptaStatsPageRoutingModule,
    BadgeModule,
    KnobModule,
    TimelineModule,
    CardModule,
    ChartModule,
    WhoscoredGlobalModule
  ],
  declarations: [OptaStatsPage, TimingStatComponent,
     FixturesStatComponent, RadarTeamStatComponent, TablePlayersComponent,
      FicheteamComponent, InfocardComponent, ClassementcardComponent,
       LastmatchescardComponent, BestscorercardComponent, BestassistcardComponent, SyntheserankcardComponent, KnearestcardComponent, ResumeStatComponent]
})
export class OptaStatsModule {}

