import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OptaStatsPage } from './opta-stats.page';

const routes: Routes = [
  {
    path: '',
    component: OptaStatsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OptaStatsPageRoutingModule {}
