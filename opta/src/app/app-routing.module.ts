import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/Fixtures',
    pathMatch: 'full'
  },
  {
    path: 'folder/Fixtures',
    loadChildren: () => import('./opta-leagues/opta-leagues.module').then( m => m.OptaLeaguesModule)
  },
  {
    path: 'folder/Players',
    loadChildren: () => import('./opta-players/opta-players.module').then( m => m.OptaPlayersModule)
  },
  {
    path: 'folder/SoccerStats',
    loadChildren: () => import('./soccer-stats/soccer-stats.module').then( m => m.SoccerStatsModule)
  },
  {
    path: 'folder/DataStats',
    loadChildren: () => import('./data-stats/data-stats.module').then( m => m.DataStatsModule)
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
