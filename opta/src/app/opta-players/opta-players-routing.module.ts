import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OptaPlayersPage } from './opta-players.page';

const routes: Routes = [
  {
    path: '',
    component: OptaPlayersPage
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OptaPlayersPageRoutingModule {}
