import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OptaPlayersPageRoutingModule } from './opta-players-routing.module';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { OptaPlayersPage } from './opta-players.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OptaPlayersPageRoutingModule
  ],
  declarations: [OptaPlayersPage]
})
export class OptaPlayersModule { }
