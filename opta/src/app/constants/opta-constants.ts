import { environment } from "src/environments/environment";

export class OptaConstants {

    /* Raspberry Endpoints*/
    public static readonly base_url_opta_info = "http://localhost:8000/opta/info";
    public static readonly base_url_opta_info_by_league = "http://localhost:8000/opta/league";
    public static readonly base_url_opta_info_by_team = "http://localhost:8000/opta/team";
    public static readonly base_url_optadb_ionic = "http://localhost:8000/optadb/ionic";
    public static readonly base_url_optadb_whoscored = "http://localhost:8000/optadb/whoscored";
    public static readonly base_url_optadb_classements = "http://localhost:8000/optadb/classements";
    public static readonly base_url_optadb_soccerstats = "http://localhost:8000/soccerstats";
    public static readonly base_url_optadb_resume = "http://localhost:8000/optadb/resume";
    public static readonly base_url_optadb_infostats = "http://localhost:8000/infostats";
    public static readonly base_url_optadb_gendata = "http://localhost:8000/gendata";
    

    /* CLOUDANT */
    public static readonly SERVICE_NAME_CLOUDANT = 'CLOUDANT';
    public static readonly dbname_teams = 'optadb_teams';
    public static readonly dbname_players = 'optadb_players';
    public static readonly dbmodel_classements = 'model_team_classements';
    public static readonly dbname_identity = 'identity_football_data_co_uk';
    public static readonly dbname_fbref_classement = 'fbref_classements';
    public static readonly dbname_fbref_head_to_head = 'fbref_head_to_head';

    /* Document Cloudant Ionic */
    
    public static readonly ui_ionic_nextmatches = 'ui_ionic_nextmatches';

     /*Seasons */
     public static readonly seasons = ['2022', '2023'];
    
     /*Leagues */
     public static readonly leagues = ['Ligue_1', 'La_liga', 'Serie_A', 'Bundesliga', 'EPL'];
     /*Time */
     public static readonly times = [
      {'key':5,'value':'5 matchs'},
      {'key':10,'value':'10 matchs'},
      {'key':20,'value':'20 matchs'},
      {'key':30,'value':'30 matchs'},
      {'key':40,'value':'40 matchs'},
      {'key':50,'value':'50 matchs'}
    ];
    /* Types */
    public static readonly types = ['ALL', 'GRAPH', 'CLASS', 'SYNTH', 'RESUME'];
    /* Bases de données Soccer Stats */ 
    public static readonly db_soccer_stats = ['wide_table','relative_form','relative_performance','record_trailing','record_leading','points_win_after_75','over_under_fulltime','current_streaks'];
    /* Identity Map Leagues */
    public static readonly identity_map_leagues = [
      {'England - Premier League':'EPL'},
      {'France - Ligue 1':'Ligue_1'},
      {'Germany - Bundesliga':'Bundesliga'},
      {'Italy - Serie A':'Serie_A'},
      {'Spain - La Liga':'La_liga'}
    ]
  
  }