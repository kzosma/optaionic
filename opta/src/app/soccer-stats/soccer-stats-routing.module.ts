import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SoccerStatsPage } from './soccer-stats.page';

const routes: Routes = [
  {
    path: '',
    component: SoccerStatsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SoccerStatsPageRoutingModule {}
