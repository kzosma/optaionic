import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { SoccerStatsPageRoutingModule } from "./soccer-stats-routing.module";
import { SoccerStatsPage } from "./soccer-stats.page";
import { CardModule } from 'primeng/card';
import { BadgeModule } from 'primeng/badge';
@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      IonicModule,
      CardModule,
      BadgeModule,
      SoccerStatsPageRoutingModule,
    ],
    declarations: [SoccerStatsPage]
  })
export class SoccerStatsModule {}