import { Component, OnInit } from '@angular/core';
import { LoadingController, AlertController, NavController } from '@ionic/angular';
import { OptaConstants } from '../constants/opta-constants';
import { OptaApiService } from '../service/opta-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-soccer-stats',
  templateUrl: './soccer-stats.page.html',
  styleUrls: ['./soccer-stats.page.scss'],
})
export class SoccerStatsPage implements OnInit {

// Styles
stylesCSS: any = {'background':'#FFFFFF', 'color': 'black', 'margin-left': '15px', 'margin-right': '15px'};
  

// VARIABLES
dbSelected: any;
leagueSelected: any;
teamHomeSelected: any;
teamHomeKNearSelected: any;
teamAwaySelected: any;
teamAwayKNearSelected: any;
loaderNextMatches: HTMLIonLoadingElement;

// CONSTANTS
// Liste des bases de données
dbs = OptaConstants.db_soccer_stats;

// DATA
// Liste des ligues extraite des bases de données
leagues_list = [];
// Données de la base en cours
database_data = [];
// Données de la ligue en cours
database_data_league = [];
// Données de l'équipe en cours
database_data_team_home = [];
database_data_team_home_knearest = [];
database_data_team_away = [];
database_data_team_away_knearest = [];
isValidate = false;

// WIDE TABLE MODELS
wide_table_total = [];
wide_table_home_away = [];

// RELATIVE FORM
relative_form_data = [];

// RELATIVE PERFORMANCE
relative_perf_data = [];

// RECORD TRAILING
record_trailing_data = [];

// RECORD LEADING
record_leading_data = [];

// POINTS WIN AFTER 75
points_win_after75_data = [];

// OVER UNDER FULLTIME
over_under_fulltime_data = [];

// CURRENT STREAKS
current_streaks_data = [];


// SERVICES
optaApi: OptaApiService;

constructor(
  private loadingCtrl: LoadingController,
  private alertController: AlertController,
  private navController: NavController,
  private router: Router,
  private optaapi: OptaApiService) { 
  this.optaApi = optaapi;
  this.showLoading('Recherche Leagues');
}
ngOnInit() {
}

initModels = () => {
  this.wide_table_total = [];
  this.wide_table_home_away = [];
  this.relative_form_data = [];
  this.relative_perf_data = [];
  this.record_trailing_data = [];
  this.record_leading_data = [];
  this.points_win_after75_data = [];
  this.over_under_fulltime_data = [];
  this.current_streaks_data = [];
}
// WIDE TABLE FUNCTIONS
build_wide_table_total = (team:any) => {
  if(!team || team.length === 0) {
    return null;
  }
  const teamData = {
     Team: team.id,
     GP: team.data.total.GP,
     Pts: team.data.total.Pts,
     W: team.data.total.W,
     D: team.data.total.D,
     L: team.data.total.L,
     PPG: team.data.total.PPG,
     GF: team.data.total.GF,
     GA: team.data.total.GA,
     WPC: Number(team.data.total.WPC_T).toFixed(2),
     PYTH: Number(team.data.total.PYTH_T).toFixed(2) 
     };
  return teamData;
}
build_wide_table_home = (team:any) => {
  if(!team || team.length === 0) {
    return null;
  }
  const teamData = {
     Team: team.id,
     GP: team.data.home.Wh + team.data.home.Lh + team.data.home.Dh,
     Pts: team.data.home.Wh *3 + team.data.home.Dh,
     W: team.data.home.Wh,
     D: team.data.home.Dh,
     L: team.data.home.Lh,
     PPG: (team.data.home.Wh *3 + team.data.home.Dh)/(team.data.home.Wh + team.data.home.Lh + team.data.home.Dh),
     GF: team.data.home.GFh,
     GA: team.data.home.GAh,
     WPC: Number(team.data.home.WPC_H).toFixed(2),
     PYTH: Number(team.data.home.PYTH_H).toFixed(2) 
     };
  return teamData;
}
build_wide_table_away = (team:any) => {
  if(!team || team.length === 0) {
    return null;
  }
  const teamData = {
    Team: team.id,
    GP: team.data.away.Wa + team.data.away.La + team.data.away.Da,
    Pts: team.data.away.Wa *3 + team.data.away.Da,
    W: team.data.away.Wa,
    D: team.data.away.Da,
    L: team.data.away.La,
    PPG: (team.data.away.Wa *3 + team.data.away.Da)/(team.data.away.Wa + team.data.away.La + team.data.away.Da),
    GF: team.data.away.GFa,
    GA: team.data.away.GAa,
    WPC: Number(team.data.away.WPC_A).toFixed(2),
    PYTH: Number(team.data.away.PYTH_A).toFixed(2)   
    };
  return teamData;
}

// RELATIVE FORM FUNCTIONS
build_relative_form = (team:any) => {
  if(!team || team.length === 0) {
    return null;
  }
  const teamData = {
    Team: team.id,
    GP: team.data.GP,
    Pts: team.data.Pts,
    ppg_all: team.data.ppg_all,
    ppg_last8: team.data.ppg_last8,
    rel_form: team.data.rel_form};
  return teamData;
}
// RELATIVE FORM FUNCTIONS
build_relative_perf = (team:any) => {
  if(!team || team.length === 0) {
    return null;
  }
  const teamData = {
    Team: team.id,
    GP: team.data.GP,
    Pts: team.data.Pts,
    ppg_opponents: team.data.ppg_opponents,
    ppg_team: team.data.ppg_team,
    rel_perf: team.data.rel_perf};
  return teamData;
}

// RECORD TRAILING FUNCTIONS
build_record_leading_trailing_data = (team:any) => {
  if(!team || team.length === 0) {
    return null;
  }
  const teamData = {
    Team: team.id,
    Pts: team.data.pts,
    W: team.data.w,
    D: team.data.d,
    L: team.data.l,
    GF: team.data.gf,
    GA: team.data.ga,
    Percentage: team.data.percentage,
    Avg_Pts: team.data.avg_pts
  };
  return teamData;
};

// POINTS WIN AFTER 75 FUNCTIONS
build_points_win_after75_data = (team:any) => {
  if(!team || team.length === 0) {
    return null;
  }
  const teamData = {
    Team: team.id,
    GP: team.data.GP,
    Pts: team.data.Pts,
    Difference: team.data.difference,
    Pts_Won: team.data.pts_won,
    Pts_Lost: team.data.pts_lost
  };
  return teamData;
  
};
// OVER UNDER FULLTIME FUNCTIONS
build_over_under_fulltime_data = (team:any) => {
  if(!team || team.length === 0) {
    return null;
  }
  const teamData = {
    Team: team.id,
    GP: team.data.GP,
    AVG: team.data.AVG,
    BTS: team.data.BTS,
    CS: team.data.CS,
    FTS: team.data.FTS,
    Plus_1_5: team.data.plus_1_5,
    Plus_2_5: team.data.plus_2_5,
    Plus_3_5: team.data.plus_3_5
  };
  return teamData;
};

// CURRENT STREAKS FUNCTIONS
build_current_streaks = (team:any) => {
  if(!team || team.length === 0) {
    return null;
  }
  const teamData = {
     Team: team.id,
     W: team.data.w,
     D: team.data.d,
     L: team.data.l,
     No_W: team.data.no_w,
     No_D: team.data.no_d,
     No_L: team.data.no_l,
     No_GF: team.data.no_gf,
     No_GA: team.data.no_ga
  };
  return teamData;
}

// APPEL SERVICE SOCCER STAT
soccer_stats_call = (dbname: string) => {
  const request = {"season":2024, "doc":'', "dbname": dbname};
  setTimeout(() => {
  this.loaderNextMatches?.present();
  this.optaApi.getOptaInfoSoccerStats(request).subscribe((data: any) => 
    {
      this.database_data = data;
      this.leagues_list = this.filter_leagues_extract_id(data);
      this.loaderNextMatches?.dismiss();
    }, err => {
      this.initModels();
      this.database_data = [];
      this.leagues_list = [];
      this.leagueSelected = '';
      this.database_data_league = [];
      this.teamHomeSelected = '';
      this.teamHomeKNearSelected = '';
      this.teamAwaySelected = '';
      this.teamAwayKNearSelected = '';
      this.database_data_team_home = [];
      this.database_data_team_home_knearest = [];
      this.database_data_team_away = [];
      this.database_data_team_away_knearest = [];
      this.loaderNextMatches?.dismiss();
    });
  },500);
}

// FILTERS DATA
filter_leagues_extract_id = (leagues, id?: string) => leagues
  .filter(league => league.id.length >0 && (id)? id === league.id:true)
  .map(league => (id)?({id: league.id, data: league.data}):({id: league.id}));

selectDB = () => {
  this.initModels();
  this.isValidate = false;
  this.database_data = [];
  this.leagues_list = [];
  this.leagueSelected = '';
  this.database_data_league = [];
  this.teamHomeSelected = '';
  this.teamHomeKNearSelected = '';
  this.teamAwaySelected = '';
  this.teamAwayKNearSelected = '';
  this.database_data_team_home = [];
  this.database_data_team_home_knearest = [];
  this.database_data_team_away = [];
  this.database_data_team_away_knearest = [];
  if(this.dbSelected && this.dbSelected.length > 0) {
    this.soccer_stats_call(this.dbSelected);
  } 
}
selectLeague = () => {
  this.isValidate = false;
  this.database_data_league = this.filter_leagues_extract_id(this.database_data, this.leagueSelected)[0].data;
}
validation = () => {
  if(this.teamHomeSelected && this.teamHomeSelected.length > 0) {
    this.database_data_team_home = this.filter_leagues_extract_id(this.database_data_league, this.teamHomeSelected)[0];
  }
  if(this.teamAwaySelected && this.teamAwaySelected.length > 0) {
    this.database_data_team_away = this.filter_leagues_extract_id(this.database_data_league, this.teamAwaySelected)[0];
  }
  if(this.teamHomeKNearSelected && this.teamHomeKNearSelected.length > 0) {
    this.database_data_team_home_knearest = this.filter_leagues_extract_id(this.database_data_league, this.teamHomeKNearSelected)[0];
  }
  if(this.teamAwayKNearSelected && this.teamAwayKNearSelected.length > 0) {
    this.database_data_team_away_knearest = this.filter_leagues_extract_id(this.database_data_league, this.teamAwayKNearSelected)[0];
  }
  this.isValidate = true;

  this.initModels();

  // Wide Table

  if(this.dbSelected === 'wide_table'){
    this.wide_table_total.push(this.build_wide_table_total(this.database_data_team_home));
    this.wide_table_total.push(this.build_wide_table_total(this.database_data_team_away));
    this.wide_table_total.push(this.build_wide_table_total(this.database_data_team_home_knearest));
    this.wide_table_total.push(this.build_wide_table_total(this.database_data_team_away_knearest));
    
    this.wide_table_home_away.push(this.build_wide_table_home(this.database_data_team_home));
    this.wide_table_home_away.push(this.build_wide_table_away(this.database_data_team_away));
    this.wide_table_home_away.push(this.build_wide_table_home(this.database_data_team_home_knearest));
    this.wide_table_home_away.push(this.build_wide_table_away(this.database_data_team_away_knearest))
  
    // Filter and sort  
    this.wide_table_total = this.wide_table_total.filter(n => {return n !== null});
    this.wide_table_total = this.wide_table_total.sort((a:any ,b: any) => {return b.Pts - a.Pts});
    this.wide_table_home_away = this.wide_table_home_away.filter(n => {return n !== null});
    this.wide_table_home_away = this.wide_table_home_away.sort((a:any ,b: any) => {return b.Pts - a.Pts});
  }
  // Relative Form
  
  if(this.dbSelected === 'relative_form'){
    this.relative_form_data.push(this.build_relative_form(this.database_data_team_home));
    this.relative_form_data.push(this.build_relative_form(this.database_data_team_away));
    this.relative_form_data.push(this.build_relative_form(this.database_data_team_home_knearest));
    this.relative_form_data.push(this.build_relative_form(this.database_data_team_away_knearest));
    // Filter and sort  
    this.relative_form_data = this.relative_form_data.filter(n => {return n !== null});
    this.relative_form_data = this.relative_form_data.sort((a:any ,b: any) => {return b.Pts - a.Pts});
  }
  // Relative Performance
  
  if(this.dbSelected === 'relative_performance'){
    this.relative_perf_data.push(this.build_relative_perf(this.database_data_team_home));
    this.relative_perf_data.push(this.build_relative_perf(this.database_data_team_away));
    this.relative_perf_data.push(this.build_relative_perf(this.database_data_team_home_knearest));
    this.relative_perf_data.push(this.build_relative_perf(this.database_data_team_away_knearest));
    // Filter and sort  
    this.relative_perf_data = this.relative_perf_data.filter(n => {return n !== null});
    this.relative_perf_data = this.relative_perf_data.sort((a:any ,b: any) => {return b.Pts - a.Pts});
  }
  
  // RECORD TRAILING
  if(this.dbSelected === 'record_trailing'){
    this.record_trailing_data.push(this.build_record_leading_trailing_data(this.database_data_team_home));
    this.record_trailing_data.push(this.build_record_leading_trailing_data(this.database_data_team_away));
    this.record_trailing_data.push(this.build_record_leading_trailing_data(this.database_data_team_home_knearest));
    this.record_trailing_data.push(this.build_record_leading_trailing_data(this.database_data_team_away_knearest));
    // Filter and sort  
    this.record_trailing_data = this.record_trailing_data.filter(n => {return n !== null});
    this.record_trailing_data = this.record_trailing_data.sort((a:any ,b: any) => {return b.Pts - a.Pts});
  }

  // RECORD LEADING
  if(this.dbSelected === 'record_leading'){
    this.record_leading_data.push(this.build_record_leading_trailing_data(this.database_data_team_home));
    this.record_leading_data.push(this.build_record_leading_trailing_data(this.database_data_team_away));
    this.record_leading_data.push(this.build_record_leading_trailing_data(this.database_data_team_home_knearest));
    this.record_leading_data.push(this.build_record_leading_trailing_data(this.database_data_team_away_knearest));
    // Filter and sort  
    this.record_leading_data = this.record_leading_data.filter(n => {return n !== null});
    this.record_leading_data = this.record_leading_data.sort((a:any ,b: any) => {return b.Pts - a.Pts});
  }
  
  // POINTS WIN AFTER 75
  if(this.dbSelected === 'points_win_after_75'){
    this.points_win_after75_data.push(this.build_points_win_after75_data(this.database_data_team_home));
    this.points_win_after75_data.push(this.build_points_win_after75_data(this.database_data_team_away));
    this.points_win_after75_data.push(this.build_points_win_after75_data(this.database_data_team_home_knearest));
    this.points_win_after75_data.push(this.build_points_win_after75_data(this.database_data_team_away_knearest));
    // Filter and sort  
    this.points_win_after75_data = this.points_win_after75_data.filter(n => {return n !== null});
    this.points_win_after75_data = this.points_win_after75_data.sort((a:any ,b: any) => {return b.Pts - a.Pts});
  }
  
  // OVER UNDER FULLTIME
  if(this.dbSelected === 'over_under_fulltime'){
    this.over_under_fulltime_data.push(this.build_over_under_fulltime_data(this.database_data_team_home));
    this.over_under_fulltime_data.push(this.build_over_under_fulltime_data(this.database_data_team_away));
    this.over_under_fulltime_data.push(this.build_over_under_fulltime_data(this.database_data_team_home_knearest));
    this.over_under_fulltime_data.push(this.build_over_under_fulltime_data(this.database_data_team_away_knearest));
    // Filter and sort  
    this.over_under_fulltime_data = this.over_under_fulltime_data.filter(n => {return n !== null});
    this.over_under_fulltime_data = this.over_under_fulltime_data.sort((a:any ,b: any) => {return b.AVG - a.AVG});
  }
  // CURRENT STREAKS
  if(this.dbSelected === 'current_streaks'){
    this.current_streaks_data.push(this.build_current_streaks(this.database_data_team_home));
    this.current_streaks_data.push(this.build_current_streaks(this.database_data_team_away));
    this.current_streaks_data.push(this.build_current_streaks(this.database_data_team_home_knearest));
    this.current_streaks_data.push(this.build_current_streaks(this.database_data_team_away_knearest));
    // Filter and sort  
    this.current_streaks_data = this.current_streaks_data.filter(n => {return n !== null});
    this.current_streaks_data = this.current_streaks_data.sort((a:any ,b: any) => {return b.No_L - a.No_L});
  }
}

async showLoading(title: string) {
  this.loaderNextMatches = await this.loadingCtrl.create({
    message: title,
    duration: 3000,
    cssClass: 'custom-loading',
  });
}

}