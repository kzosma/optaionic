import { Component, OnInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  public appPages = [
    { title: 'OptaStat', url: '/folder/Fixtures', icon: 'football' },
    { title: 'SoccerStat', url: '/folder/SoccerStats', icon: 'football' },
    { title: 'FBRef', url: '/folder/DataStats', icon: 'layers' },
    { title: 'NBA', url: '/folder/ModelSorare', icon: 'basketball' },
    { title: 'MLB', url: '/folder/Players', icon: 'baseball' }
    
    
  ];
  constructor(private primengConfig: PrimeNGConfig) {}
  ngOnInit(): void {
    this.primengConfig.ripple = true;
  }

}
