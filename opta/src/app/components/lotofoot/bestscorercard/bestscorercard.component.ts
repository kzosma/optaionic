import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-bestscorercard',
  templateUrl: './bestscorercard.component.html',
  styleUrls: ['./bestscorercard.component.scss'],
})
export class BestscorercardComponent implements OnInit {

  @Input()
  data:any;

  constructor() { }
  ngOnInit() {}
}
