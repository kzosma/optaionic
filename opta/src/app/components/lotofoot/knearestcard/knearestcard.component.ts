import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-knearestcard',
  templateUrl: './knearestcard.component.html',
  styleUrls: ['./knearestcard.component.scss'],
})
export class KnearestcardComponent implements OnInit {

  @Input()
  data:any;

  constructor() { }
  ngOnInit() {}

}
