import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-classementcard',
  templateUrl: './classementcard.component.html',
  styleUrls: ['./classementcard.component.scss'],
})
export class ClassementcardComponent implements OnInit {

  @Input()
  data:any;
  
  constructor() { }

  ngOnInit() {}

}
