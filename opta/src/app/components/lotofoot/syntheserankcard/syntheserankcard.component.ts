import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-syntheserankcard',
  templateUrl: './syntheserankcard.component.html',
  styleUrls: ['./syntheserankcard.component.scss'],
})
export class SyntheserankcardComponent implements OnInit {

  @Input()
  data:any;
  constructor() { }
  ngOnInit() {}

}
