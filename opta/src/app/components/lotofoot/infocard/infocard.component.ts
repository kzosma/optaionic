import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-infocard',
  templateUrl: './infocard.component.html',
  styleUrls: ['./infocard.component.scss'],
})
export class InfocardComponent implements OnInit {

  @Input()
  data:any;
  
  stylesInfoHome: any = {'background':'#fbe4ca', 'color': 'black', 'margin-left': '15px', 'margin-right': '15px'};
  stylesInfoAway: any = {'background':'#d6dde6', 'color': 'black', 'margin-left': '15px', 'margin-right': '15px'};

  constructor() { }

  ngOnInit() {}



}
