import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-ficheteam',
  templateUrl: './ficheteam.component.html',
  styleUrls: ['./ficheteam.component.scss'],
})
export class FicheteamComponent implements OnInit {

  @Input()
  data:any;

  constructor() { }

  ngOnInit() {}

}
