import { Component, Input, OnInit } from '@angular/core';
import { PrimeIcons } from 'primeng/api';

@Component({
  selector: 'app-resume-stat',
  templateUrl: './resume-stat.component.html',
  styleUrls: ['./resume-stat.component.scss'],
})
export class ResumeStatComponent implements OnInit {

  @Input()
  dataHome:any;
  
  @Input()
  dataAway:any;

  // Styles
  stylesCSS: any = {'background':'#FFFFFF', 'color': 'black', 'margin-left': '15px', 'margin-right': '15px'};
  
  // Data selection
  dayHomeSelected: any;
  daysHome: number[] = [];
  dayAwaySelected: any;
  daysAway: number[] = [];
  
  // Data affichage
  
  // Formation
  formationHome : any;
  formationAway : any;
  
  // Roster
  rosterHome : any;
  rosterAway : any;

  // Timing
  timingHome : any;
  timingAway : any;

  // Situation
  situationHome : any;
  situationAway : any;

  // Game State
  gamestateHome : any;
  gamestateAway : any;

  // Styles
  styleArrayHome = {
    'bgcolor': '#231f20',
    'headercolor': '#008bcd',
    'textcolor': '#FFFFFF',
    'highlightcolor': '#008bcd',
    'subtitle': new Date().toLocaleDateString(),
    'title': ''
  }
  styleArrayAway = {
    'bgcolor': '#231f20',
    'headercolor': '#008bcd',
    'textcolor': '#FFFFFF',
    'highlightcolor': '#FFFF00',
    'subtitle': new Date().toLocaleDateString(),
    'title': ''
  }
 
  constructor() { }
  ngOnInit() {
    if(this.dataHome.data.length > 0) {
      for (let i = 0; i <= this.dataHome.data.length; i++) {
        this.daysHome.push(i);
      }
    }
    if(this.dataAway.data.length > 0) {
      for (let i = 0; i <= this.dataAway.data.length; i++) {
        this.daysAway.push(i);
      }
    }
  }
  reset = () => {
    this.dayHomeSelected = undefined;
    this.dayAwaySelected = undefined;
    this.formationHome = this.formationAway = this.situationHome = this.situationAway = this.timingHome = this.timingAway = this.rosterHome = this.rosterAway = undefined;
  }
  validation() {
    if(this.dayHomeSelected !== undefined) {
      let dataHomeFilterById = this.getTeamDataById(this.dataHome, this.dayHomeSelected);
      const h_a = dataHomeFilterById.id_h === this.dataHome.id ? 'h': 'a';
      this.formationHome = this.getFormation(dataHomeFilterById, h_a);
      this.rosterHome = this.getRoster(dataHomeFilterById, h_a);
      this.situationHome = this.getSituation(dataHomeFilterById, h_a);
      this.timingHome = this.getTiming(dataHomeFilterById, h_a);
      this.gamestateHome = this.getGameState(dataHomeFilterById, h_a);

    }
    if(this.dayAwaySelected !== undefined) {
      let dataAwayFilterById = this.getTeamDataById(this.dataAway, this.dayAwaySelected);
      const h_a = dataAwayFilterById.id_h === this.dataAway.id ? 'h': 'a';
      this.formationAway = this.getFormation(dataAwayFilterById, h_a);
      this.rosterAway = this.getRoster(dataAwayFilterById, h_a);

      this.situationAway = this.getSituation(dataAwayFilterById, h_a);
      this.timingAway = this.getTiming(dataAwayFilterById, h_a);
      this.gamestateAway = this.getGameState(dataAwayFilterById, h_a);

    }
  }
  selectDayHome() {}
  selectDayAway() {}
  
  getFormation(data: any, h_a: string) {
    if(h_a === 'h') {
      return data.formation_h;
    }
    else {
      return data.formation_a;
    }
  }

  getRoster(data: any, h_a: string) {
    if(h_a === 'h') {
      return data.roster_h;
    }
    else {
      return data.roster_a;
    }
  }

  getSituation(data: any, h_a: string) {
    if(h_a === 'h') {
      return data.situation_h;
    }
    else {
      return data.situation_a;
    }
  }

  getTiming(data: any, h_a: string) {
    if(h_a === 'h') {
      return data.timing_h;
    }
    else {
      return data.timing_a;
    }
  }

  getGameState(data: any, h_a: string) {
    if(h_a === 'h') {
      return data.gamestate_h;
    }
    else {
      return data.gamestate_a;
    }
  }
  
  getTeamDataById(dataTeam: any, id: string): any[] | any {
   let matches = []; 
   if(id === '0') {
    dataTeam.data.forEach((item, index) => {
      matches.push(this.getTeamDataById(dataTeam,item.id));
     });
    }
    else {
        const matchesById = dataTeam.data.filter(match => (match.id === id));
        matches = matchesById[0].data.filter(match => (this.transformTeamName(match.h) === dataTeam.title || this.transformTeamName(match.a) === dataTeam.title));
    }
   if(id !== '0') {
    return matches[0];
   }
   return matches;
  }
  transformTeamName(teamName: string): string {
    return teamName.split(' ').join('_');
  }
  calculateSum(a: string, b: string): string {
    const sum = parseFloat(a) + parseFloat(b);
    return sum.toFixed(2);
  }
}
