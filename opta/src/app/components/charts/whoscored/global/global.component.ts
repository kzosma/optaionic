import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.scss'],
})
export class GlobalComponent implements OnInit {

  // VARIABLES
  @Input()
  dataHome: any;

  @Input()
  dataAway: any;

  // Styles
  stylesCSS: any = {'background':'#FFFFFF', 'color': 'black', 'margin-left': '15px', 'margin-right': '15px'};
 
  constructor() {
  }
  ngOnInit() {}
}
