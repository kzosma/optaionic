import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { BadgeModule} from 'primeng/badge';
import {KnobModule} from 'primeng/knob';
import {TimelineModule} from 'primeng/timeline';
import {CardModule} from 'primeng/card';
import {ChartModule} from 'primeng/chart';
import { GlobalComponent } from '../whoscored/global/global.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BadgeModule,
    KnobModule,
    TimelineModule,
    CardModule,
    ChartModule
  ],
  declarations: [GlobalComponent],
  exports: [GlobalComponent]
})
export class WhoscoredGlobalModule {}

