import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-fixtures-stat',
  templateUrl: './fixtures-stat.component.html',
  styleUrls: ['./fixtures-stat.component.scss'],
})
export class FixturesStatComponent implements OnInit {

  @Input()
  data:any;
  
  // Styles
  stylesCSS: any = {'background':'#FFFFFF', 'color': 'black', 'margin-left': '15px', 'margin-right': '15px'};
  
  // Constructeur
  constructor() {}
  // INIT
  ngOnInit() {}


}
