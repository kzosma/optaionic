import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table-players',
  templateUrl: './table-players.component.html',
  styleUrls: ['./table-players.component.scss'],
})
export class TablePlayersComponent implements OnInit {

  @Input()
  data:any;


  
  // Styles
  stylesCSS: any = {'background':'#FFFFFF', 'color': 'black', 'margin-left': '15px', 'margin-right': '15px'};
  
  constructor() { }

  ngOnInit() {}

}
