import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-radar-team-stat',
  templateUrl: './radar-team-stat.component.html',
  styleUrls: ['./radar-team-stat.component.scss'],
})
export class RadarTeamStatComponent implements OnInit {

  @Input()
  data:any;
  

  // Styles
  stylesCSS: any = {'background':'#FFFFFF', 'color': 'black', 'margin-left': '15px', 'margin-right': '15px'};
  stylesRadar: any = {'width': '90%'}; 

  // ChartOptions
  chartOptions: any = this.getLightTheme();

  // Constructeur
  constructor() {}
  // INIT
  ngOnInit() {}

  // GET LIGHT THEME
  getLightTheme() {
    return {
        plugins: {
            legend: {
                labels: {
                    color: '#495057'
                }
            }
        },
        scales: {
            r: {
                pointLabels: {
                    color: '#495057'
                },
                grid: {
                    color: '#ebedef'
                },
                angleLines: {
                    color: '#ebedef'
                }
            }
        }
    };
}

}
