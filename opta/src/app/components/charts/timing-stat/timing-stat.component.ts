import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-timing-stat',
  templateUrl: './timing-stat.component.html',
  styleUrls: ['./timing-stat.component.scss'],
})
export class TimingStatComponent implements OnInit {

  @Input()
  data:any;
  
  // Styles
  stylesCSS: any = {'background':'#FFFFFF', 'color': 'black', 'margin-left': '15px', 'margin-right': '15px'};
  
  // Constructeur
  constructor() {}
  // INIT
  ngOnInit() {}
}
