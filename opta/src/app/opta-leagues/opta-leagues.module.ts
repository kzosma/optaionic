import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { OptaLeaguesPageRoutingModule } from './opta-leagues-routing.module';
import { OptaLeaguesPage } from './opta-leagues.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OptaLeaguesPageRoutingModule
  ],
  declarations: [OptaLeaguesPage]
})
export class OptaLeaguesModule {}

