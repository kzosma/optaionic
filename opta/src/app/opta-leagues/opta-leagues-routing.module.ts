import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OptaStatsPage } from '../opta-stats/opta-stats.page';

import { OptaLeaguesPage } from './opta-leagues.page';

const routes: Routes = [
  {
    path: '',
    component: OptaLeaguesPage
  },
  {
    path: 'stats',
    loadChildren: () => import('../opta-stats/opta-stats.module').then(m => m.OptaStatsModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OptaLeaguesPageRoutingModule {}
