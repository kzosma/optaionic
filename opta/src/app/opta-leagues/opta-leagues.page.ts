import { Component, OnInit } from '@angular/core';
import { LoadingController, AlertController, NavController } from '@ionic/angular';
import { OptaConstants } from '../constants/opta-constants';
import { OptaApiService } from '../service/opta-api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-opta-leagues',
  templateUrl: './opta-leagues.page.html',
  styleUrls: ['./opta-leagues.page.scss'],
})
export class OptaLeaguesPage implements OnInit {
  
  // VARIABLES
  startDay: any;
  leagueSelected: any;
  matchsSelected: any;
  typeSelected: any = OptaConstants.types[0];
  loaderNextMatches: HTMLIonLoadingElement;
  dataNextMatches = [];
  dataNextMatchesFilter = [];
  homeid: number;
  awayid: number;
  
  // CONSTANTS
  leagues = OptaConstants.leagues;
  times = OptaConstants.times;
  types = OptaConstants.types;
   
  // SERVICES
  optaApi: OptaApiService;


  constructor(
    private loadingCtrl: LoadingController,
    private alertController: AlertController,
    private navController: NavController,
    private router: Router,
    private optaapi: OptaApiService) { 
    this.startDay = new Date().toISOString();
    this.matchsSelected = 5;
    this.optaApi = optaapi;
    this.showLoading('Recherche prochains matchs');
  }
  ngOnInit() {
    setTimeout(() => { 
      this.loaderNextMatches?.present();
      const body = {"season":2023, "doc":OptaConstants.ui_ionic_nextmatches};
      this.optaApi.getOptaInfoIonic(body).subscribe((data: any) => 
      {
        for(let league of data.data) {
          const nextMatches = league.nextMatches;
          for(let match of nextMatches) {
            this.dataNextMatches.push({'league': league.league, 'idMatch': match.id, 'home': match.home, 'away': match.away, 'dateMatch': match.datetime});
          }
        }
        this.dataNextMatchesFilter = this.dataNextMatches.sort((objA, objB) => new Date(objA.dateMatch).getTime() - new Date(objB.dateMatch).getTime());
        this.dataNextMatchesFilter =  this.dataNextMatchesFilter.filter((value) => new Date(value.dateMatch).getTime() > new Date().getTime()).slice(0,this.matchsSelected);
        this.loaderNextMatches.dismiss();
      }, err => {
        this.loaderNextMatches.dismiss();
      });
    }, 500);
  }
  nextDays = (dateString: string) => {
    let yesterday=new Date(new Date().getTime() - (1 * 24 * 60 * 60 * 1000));
    const date = new Date(dateString);
    return date >= yesterday;
 };
  selectDate = () => {
    this.leagueSelected = 'All';
    this.matchsSelected = 5;
    this.dataNextMatchesFilter =  this.dataNextMatches.filter((value) => new Date(value.dateMatch).getTime() > new Date(this.startDay).getTime()).slice(0,this.matchsSelected);
    this.dataNextMatchesFilter = this.dataNextMatchesFilter.sort((objA, objB) => new Date(objA.dateMatch).getTime() - new Date(objB.dateMatch).getTime());
        
  }
  selectLeague = () => {
    if(this.leagueSelected === 'All') {
      this.dataNextMatchesFilter =  this.dataNextMatches.filter((value) => (new Date(value.dateMatch).getTime() > new Date(this.startDay).getTime())).slice(0,this.matchsSelected);
    } else {
      this.dataNextMatchesFilter =  this.dataNextMatches.filter((value) => (value.league === this.leagueSelected && new Date(value.dateMatch).getTime() > new Date(this.startDay).getTime())).slice(0,this.matchsSelected);
    }
  }
  selectMatchs =() => {
    if(this.leagueSelected === 'All') {
      this.dataNextMatchesFilter =  this.dataNextMatches.filter((value) => (new Date(value.dateMatch).getTime() > new Date(this.startDay).getTime())).slice(0,this.matchsSelected);
    } else {
      this.dataNextMatchesFilter =  this.dataNextMatches.filter((value) => (value.league === this.leagueSelected && new Date(value.dateMatch).getTime() > new Date(this.startDay).getTime())).slice(0,this.matchsSelected);
    }
  }
  selectTypes =() => {
    console.log(this.typeSelected);
  }
  async showLoading(title: string) {
    this.loaderNextMatches = await this.loadingCtrl.create({
      message: title,
      duration: 3000,
      cssClass: 'custom-loading',
    });
  }

  async navigateToFixtureMatch(home, away) {
    this.router.navigate(['/folder/Fixtures/stats',{'home': home.id, 'away': away.id, 'type': this.typeSelected}]);
  }
  reset = () => {
    this.homeid = undefined;
    this.awayid = undefined;
  }
  validation = () => {
    if(this.homeid && this.awayid) {
      this.router.navigate(['/folder/Fixtures/stats',{'home': this.homeid, 'away': this.awayid, 'type': this.typeSelected}]);
    }
  }
}
